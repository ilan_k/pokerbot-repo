#include "CommunityCards.h"
#include <iostream>
#include <sstream>

bool CommunityCards::addToCommunity(){
   if(deck->size() > 0){
      community.push_back(deck->dealTopCard());
      return true;
   }
   else{
      cout << "ERROR: Tried adding card to community cards, but no cards left" << endl;
      return false;
   }
}

bool CommunityCards::addToBurn(){
   if(deck->size() > 0){
      burnCards.push_back(deck->dealTopCard());
      return true;
   }
   else{
      cout << "ERROR: Tried adding card to burn cards, but no cards left" << endl;
      return false;
   }
}

//dealFlop deals 3 cards into community
//as long as flop is false
bool CommunityCards::dealFlop(){
     if(!flop && deck != NULL){
        flop = true;    
               
        //adds a card to burn, then adds 3 community cards
        //Only does any of the actions if the prior one worked
        return (addToBurn() && addToCommunity() && addToCommunity() && addToCommunity());
     }
     else{
        cout << "ERROR: Could not deal flop" << endl;
        return false;
     }
}
   
//dealTurn deals 1 card into community
//as long as turn is false
bool CommunityCards::dealTurn(){
     if(flop && !turn && deck != NULL){
        turn = true; 
     
        //adds a card to burn, then adds 1 community cards
        //Only does any of the actions if the prior one worked
        return addToBurn() && addToCommunity();
     }
     else{
        cout << "ERROR: Could not deal turn" << endl;
        return false;
     }
}
   
//dealRiver deals 1 card into community
//as long as river is false
bool CommunityCards::dealRiver(){
     if(flop && turn && !river && deck != NULL){
        river = true;           
            
        //adds a card to burn, then adds 1 community cards
        //Only does any of the actions if the prior one worked   
        return addToBurn() && addToCommunity(); 
     }
     else{
        cout << "ERROR: Could not deal river" << endl;
        return false;
     }
}

//Returns back a string of all the community cards
//[Xx, Xx, ...]
string CommunityCards::toString(){
   if(community.size() > 0){
      stringstream ss;
   
      ss << "[";
   
      //community is stored in backwards order
      for(int ind = 0; ind < community.size() - 1; ind++){
         ss << community[ind].toString() << ", ";
      }
   
      ss << community[community.size() - 1].toString() << "]";
   
      return ss.str();
   }
   else{
      return "";
   }

}

void CommunityCards::toStringGraphic(vector<string> &text, int indent){
   vector< vector<string> > graphicalCommunity;
   
   for(int ind = 0; ind < community.size(); ind++){
      vector<string> graphicCard;     
           
      community[ind].toStringGraphic(graphicCard);

      graphicalCommunity.push_back(graphicCard);
   }
   
   string line;
   
   //For each line of every card in the hand, store it into text 
   for(int i = 0; i < (graphicalCommunity[0]).size(); i++){
      line = "";
      
      for(int j = 0; j < indent; j++){
         line.append(" ");
      }
      
      for(int j = 0; j < graphicalCommunity.size(); j++){
         line.append((graphicalCommunity[j])[i]);
         
         if(j < graphicalCommunity.size() - 1){
            line.append(" ");
         }
      }
      
      text.push_back(line);
   }
}
   
//Returns back a string of just the flop
//[Xx, Xx, Xx]
string CommunityCards::flopString(){
   if(flop){
      stringstream ss;
      
      ss << "[";
      
      for(int ind = 0; ind < 2; ind++){
         ss << community[ind].toString() << ", ";
      }
      
      ss << community[2].toString() << "]";
   
      return ss.str();
   }
   else{
      return "";
   }
}
   
//[Xx]
string CommunityCards::turnString(){
   if(turn){
      stringstream ss;
      
      ss << "[" << community[3].toString() << "]";
      
      return ss.str();
   }
   else{
      return "";
   }
}
   
//[Xx]
string CommunityCards::riverString(){
   if(river){
      stringstream ss;
   
      ss << "[" << community[4].toString() << "]";
   
      return ss.str();
   }
   else{
      return "";
   }
}
    
//Returns a COPY of the community cards
//Actual community cards cannot be changed
vector<Card> CommunityCards::getCommunity(){
   vector<Card> data(community.begin(), community.end());
   return data;
}
