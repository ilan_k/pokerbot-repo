#include "PokerTable.h"

//delete these after done testing
#include <iostream>
#include <iomanip>
#include <locale>

#define MINIMUM_BUY_IN bigBlind * 35
#define MAXIMUM_BUY_IN bigBlind * 100

#include "testing.h"

using namespace std;

void PokerTable::initializeSeats(){
   for(int ind = 0; ind < seats; ind++){
      players.push_back(SeatedPlayer()); 
   }
}

void PokerTable::initializeTable(unsigned int s, unsigned int small, unsigned int big){
   seats = (s <= MAX_SEATS) ? s : MAX_SEATS;
   smallBlind = small;
   bigBlind = big;
   dealOver = true;
   numberOfPlayers = 0;
   comm = NULL;
   deck = NULL;
   pot = 0;  
   actionIndex = -1;
   buttonIndex = -1;
   twoBigBlinds = false;
   
   initializeSeats();
}  

void PokerTable::sitOutNoChips(vector<SeatedPlayer*> *playersNowSittingOut){
   if(playersNowSittingOut != NULL) playersNowSittingOut->clear();  
     
   for(int ind = 0; ind < players.size(); ind++){
      if(players[ind].getName() != "" && !players[ind].sitout && players[ind].totalChips() == 0){
         players[ind].sitout = true;
         
         if(playersNowSittingOut != NULL) playersNowSittingOut->push_back(&(players[ind]));
      } 
   }
}

bool PokerTable::moveButton(vector<int> &activePlayers){
   
   if(DEBUG_TABLE){ cout << "In moveButton" << endl; system("PAUSE"); }   
   
   int ind = 0;
      
   //Find next active player able to hold the button
   while(activePlayers[ind] <= buttonIndex && ind != activePlayers.size()){
      ind++;
   }
   
   if(DEBUG_TABLE) { cout << "Past while(activePlayers[ind] <= buttonIndex || ind == activePlayers.size())" << endl; cout << "ind is " << ind << endl; cout << "activePlayers.size() is " << activePlayers.size() << endl; system("PAUSE");}
      
   if(ind == activePlayers.size()){
      ind = 0;
   }
      
   int nextActivePlayer = ind; // store this player
   bool buttonIndFound = false;
      
   //Find the next active player 
   do{
      cout << "ind is " << ind << endl;    
      //Only a player that is not a new player can hold the button next
      //IE a player that has already played at least 1 hand on the table
      //Also disallow a player that has missed a blind previously to post on the button
      if(!players[activePlayers[ind]].newPlayer && players[activePlayers[ind]].missedBlind == NONE){
         buttonIndFound = true;
         break; //Current index is the next available person that is allowed to hold button
      }
         
      ind++;
         
      if(ind == activePlayers.size()){
         ind = 0;
      }
   //If it reaches this condition, that means nobody could be the button
   //If the person who was button last, is the only non-new player at the table
   //He retains the button for this next hand
   } while(ind != nextActivePlayer); 
   
   if(DEBUG_TABLE){ cout << "Past while(ind != nextActivePlayer);" << endl; system("PAUSE"); }
   
   if(buttonIndFound){
      buttonIndex = activePlayers[ind];
      return true;
   }
   else{
      //Button could not be moved!
      //Reason for this is if all players are now a new player,
      //then need to redraw the button   
      return false;
   }
}

void PokerTable::giveMissedBlinds(int bigBlindIndex){
   for(int ind = buttonIndex + 1; ; ind++){
      if(ind == players.size()){
         ind = 0;
      }
      
      if(ind == bigBlindIndex){
         break;
      }
      
      if(players[ind].getName() != "" && players[ind].sitout){
         if(players[ind].isBlind == BIG_BLIND){ //Last hand the player was a big blind
             players[ind].missedBlind = SMALL_BLIND;
         }
         else if(players[ind].isBlind == NONE){ //Last hand the player was under the gun, -- now the player has missed both blinds
             players[ind].missedBlind = BOTH;
         }
      }
   }  
}

void PokerTable::postBigBlind(int bigBlindIndex){
   players[bigBlindIndex].isBlind = BIG_BLIND;
   players[bigBlindIndex].newPlayer = false;
   players[bigBlindIndex].missedBlind = NONE;   //Players that missed both big and small blinds previously, now have missed none
   players[bigBlindIndex].betChips(bigBlind);
}

void PokerTable::postSmallBlind(int smallBlindIndex){
   players[smallBlindIndex].isBlind = SMALL_BLIND;
   players[smallBlindIndex].newPlayer = false;
   players[smallBlindIndex].missedBlind = NONE;   //Players that missed both big and small blinds previously, now have missed none
   players[smallBlindIndex].betChips(smallBlind);
}

//It is assumed that the right most index in bigBlinds is the right most big blind
//in the game
void PokerTable::initializeBlinds(const vector<int> &bigBlinds, const vector<int> &smallBlinds){
   //Remove all blind information from last hand
   for(int ind = 0; ind < players.size(); ind++){
      players[ind].isBlind = NONE;
   }
   
   //Post all big blinds
   for(int ind = 0; ind < bigBlinds.size(); ind++){
      postBigBlind(bigBlinds[ind]);
   }
   
   twoBigBlinds = (bigBlinds.size() == 2);

   //Post all small blinds
   for(int ind = 0; ind < smallBlinds.size(); ind++){
      postSmallBlind(smallBlinds[ind]);
   }
   
   //give out missed blinds for the hand
   //All players that did not post between the button and the right most big blind get
   //missed blinds
   giveMissedBlinds(bigBlinds[bigBlinds.size() - 1]);
}

//p1 is a player1 index in activePlayers
//p2 is a player2 index in activePlayers
//Assumed that p1 or p2 has the button
void PokerTable::postBlindsHeadsUp(vector<int> &activePlayers, int p1, int p2){
   int btn = (activePlayers[p1] == buttonIndex) ? p1 : p2;
   int bb = (activePlayers[p1] == buttonIndex) ? p2 : p1;
            
   vector<int> bbs;
   bbs.push_back(bb);
   
   vector<int> sbs;
   sbs.push_back(btn);         
   
   initializeBlinds(bbs, sbs);
            
   headsUp = true; 
}

void PokerTable::postBlindsHeadsUp(vector<int> &activePlayers){
   vector<int> headsUp;
   
   for(int ind = 0; ind < activePlayers.size() || players.size() == 2; ind++){
      int blind = players[activePlayers[ind]].isBlind;     
           
      if(blind == BIG_BLIND || blind == SMALL_BLIND){
         headsUp.push_back(ind);
      }
   }
   
   postBlindsHeadsUp(activePlayers, headsUp[0], headsUp[1]);
}

bool PokerTable::headsUpPlayersHere(vector<int> &activePlayers, int &bb){
   bool btnActive = players[buttonIndex].isBlind == SMALL_BLIND;
   bool bbActive = false;
         
   for(int ind = 0; ind < activePlayers.size(); ind++){
      if(activePlayers[ind] == buttonIndex){
         continue;
      }
            
      int j = activePlayers[ind];
            
      if(players[j].isBlind == BIG_BLIND){
         bbActive = true;
         bb = ind;
         break;
      }
   }
   
   return btnActive && bbActive;
}

bool PokerTable::conversionFromHeadsUp(vector<int> &activePlayers){
   //Before moving the button, check to see if moving button is needed
   //In the case that it was previously heads up, and converting to multi players, do not move button
   if(headsUp){        
      if(activePlayers.size() > 2){
         int currentbb;
          
         if(headsUpPlayersHere(activePlayers, currentbb)){
            //buttonIndex = person currently on the button
            //currentbb = person currently big blind.
            //check to see if there is a person that can post, that is to the right of the bb
            int nextPlayer = (currentbb + 1 == activePlayers.size()) ? 0 : currentbb + 1;
            
            if(activePlayers[nextPlayer] != buttonIndex){ //The next person that can play is not the button! Convert to non-heads up game
               vector<int> bbs;
               bbs.push_back(activePlayers[nextPlayer]);
            
                vector<int> sbs;
               sbs.push_back(activePlayers[currentbb]);
            
               initializeBlinds(bbs, sbs);
               
               if(DEBUG_TABLE){ cout << "sb is " << players[activePlayers[currentbb]].totalBet() << endl;
               cout << "bb is " << players[activePlayers[nextPlayer]].totalBet() << endl;}
            
               headsUp = false;
               return true;   
            }
         }
         else{
            //End heads up
            //Button is always moved to nearest non-new player
            //If BB left, then button will remain the same, -- if button left, button will move to BB
            headsUp = false;
         }
      }
   }
   
   return false;   
}

bool PokerTable::moveButtonPostBlinds(vector<int> &activePlayers, bool *randomizedButton){
   //Button has not been assigned yet
   if(buttonIndex == -1){
      //initialize first hand, - Set button, and blinds
      initializeFirstHand(activePlayers, randomizedButton);
   }
   else{  
      //Before moving the button, check to see if moving button is needed
      //In the case that it was previously heads up, and converting to multi players, do not move button      
      bool conversion = conversionFromHeadsUp(activePlayers);
              
      if(DEBUG_TABLE){ cout << "conversion is " << conversion << endl; cout << "buttonIndex is " << buttonIndex << endl; }     
              
      //If conversion is set to true, it will not execute the if statement        
      if(!conversion && moveButton(activePlayers)){  
         if(DEBUG_TABLE) cout << "inside if statement, buttonIndex is " << buttonIndex << endl;
                          
         //Button has successfully moved to a player that has previously played at least 1 hand                       
         if(activePlayers.size() == 2 || headsUp){
            headsUp = true;
            postBlindsHeadsUp(activePlayers);           
         }
         //Non-Heads up
         else{                                                                                                    
            int bigBlindCount = 0;
            int smallBlindCount = 0;
            
            //Determine which blinds from the last hand
            //are still present in this hand
            for(int ind = 0; ind < activePlayers.size(); ind++){
               if(players[activePlayers[ind]].isBlind == BIG_BLIND){
                  bigBlindCount++;
               }
               else if(players[activePlayers[ind]].isBlind == SMALL_BLIND){
                  smallBlindCount++;
               }
            }         
         
            //Determine the blind for this hand,
            //either is small/big, or big/big
            if(twoBigBlinds){ //If there was 2 big blinds last hand
               twoBigBlinds = (bigBlindCount != 2);   //One of the people that posted big/big left the game, so need to do big-big again    
            }
            else{ //Last hand the structure was small/big
               twoBigBlinds = (bigBlindCount == 0 || smallBlindCount == 0); //One of the small blind, or one of the big blind, or both, left
            }         
         
            //Make the button post, if he missed a blind
            if(players[buttonIndex].isBlind == BIG_BLIND){
               //The person who is now dealer missed his small blind. Post it
               players[buttonIndex].betChips(smallBlind);
            }
            else if(players[buttonIndex].isBlind == NONE){
               //The button missed BOTH his Big blind + small blind
               //Post a big blind, and forget about him posting a small blind
               players[buttonIndex].betChips(bigBlind);
            }
         
            players[buttonIndex].isBlind = NONE; //This shows that the player has now posted both his blinds
            
            vector<int> bbs;
            vector<int> sbs;
            
            //Big/Big for this hand
            if(twoBigBlinds){ //The first two people to the right of the button post a bigblind -- (even new players)
               int p1 = (buttonIndex + 1 == players.size()) ? 0 : buttonIndex + 1;
               int p2 = (p1 + 1 == players.size()) ? 0 : p1 + 1;
               
               bbs.push_back(p1); bbs.push_back(p2);
               initializeBlinds(bbs, sbs);
            }
            //Small/Big for this hand
            else{ //The first player that posts a small blind must NOT be a new player (he/she will have to sit out)
               int btn = 0;
               int p1;
               int p2;
               
               for( ; btn < activePlayers.size(); btn++){
                  if(buttonIndex == activePlayers[btn]){
                     break;
                  }
               }           
               
               if(btn == activePlayers.size()){ //BUG here
                  cout << "Error occured in Small/Big posting @ btn == activePlayer.size()" << endl;
                  system("PAUSE");
               }
               
               //Find small blind
               for(int ind = btn + 1; ; ind++){
                  if(ind == activePlayers.size()){
                     ind = 0;
                  }
                  
                  if(ind == btn){
                     cout << "Error occured in loop! @ if(ind == btn)" << endl;
                     system("PAUSE");
                  }
                  
                  if(!players[activePlayers[ind]].newPlayer && players[activePlayers[ind]].isBlind == BIG_BLIND){ //Players posting a small must have been big last hand
                     p1 = activePlayers[ind];
                     break;
                  }
               }
               
               p2 = (p1 + 1 == activePlayers.size()) ? 0 : p1 + 1;
               
               if(p2 == buttonIndex){
                  cout << "Error occured! p2 == buttonIndex" << endl;
                  system("PAUSE");
               }
               
               bbs.push_back(p2); sbs.push_back(p1);
               
               initializeBlinds(bbs, sbs);
            }
         }
      }
      else{
         //Button could not be moved (No non-new player on the table)
         initializeFirstHand(activePlayers, randomizedButton);
      }  
   } 
   
   if(DEBUG_TABLE) cout << "before: return true; buttonIndex is " << buttonIndex << endl;
   return true;
}

bool PokerTable::initializeFirstHand(vector<int> &activePlayers, bool *randomizedButton){
   int activePlayerBtn;
   
   //Initialize Button
   if(initializeButton(activePlayers, &activePlayerBtn, randomizedButton) != -1){
      //buttonIndex has now been set
      //activePlayerBtn is the index of that player
      
      //Set all players at the table as non-new players
      for(int ind = 0; ind < activePlayers.size(); ind++){
         players[activePlayers[ind]].newPlayer = false;
         players[activePlayers[ind]].missedBlind = NONE;
         players[activePlayers[ind]].isBlind = NONE;
      }
      
      if(activePlayers.size() == 2){
         headsUp = true;
         postBlindsHeadsUp(activePlayers, 0, 1);
      }
      else{
         headsUp = false;
         
         int smallBlindInd = (activePlayerBtn + 1 == activePlayers.size()) ? 0 : activePlayerBtn + 1;
         postSmallBlind(activePlayers[smallBlindInd]);
         
         int bigBlindInd = (smallBlindInd + 1 == activePlayers.size()) ? 0 : smallBlindInd + 1;
         postBigBlind(activePlayers[bigBlindInd]);
      }
      
      twoBigBlinds = false; 

      return true;
   }
   //Not enough players could start the hand
   else{
      return false;
   }
}

int PokerTable::initializeButton(vector<int> &activePlayers, int *activePlayerBtn, bool *randomizedButton){
   buttonIndex = -1; 
    
   if(activePlayers.size() >= 2){
      //Pick random active player to be the button
      srand(time(NULL));
      
      int index = rand() % activePlayers.size();
      
      buttonIndex = activePlayers[index]; 
      
      if(activePlayerBtn != NULL) (*activePlayerBtn) = index;
      
      if(randomizedButton != NULL) (*randomizedButton) = true;
   }
   
   return buttonIndex;
}

void PokerTable::dealHandToPlayer(SeatedPlayer * sp){
   Card c1 = deck->dealTopCard();
   Card c2 = deck->dealTopCard();         
         
   sp->setHand(HoldemHand(c1, c2));
   if(sp->newPlayer) sp->newPlayer = false; 
    
   if(sp->isBlind != BIG_BLIND && sp->isBlind != SMALL_BLIND){   
      switch(sp->missedBlind){
         case SMALL_BLIND:
            sp->betChips(smallBlind);
            break;
         case BOTH:
            sp->betChips(bigBlind);
            break;
      }
   }
         
   sp->missedBlind = NONE;      
   playersInDeal.push_back(sp); 
}
  
bool PokerTable::shuffleUpAndDeal(vector<SeatedPlayer*> *playersNowSittingOut, bool *randomizedButton){
   clearDeck();
   
   clearCommunity();

   //Ensure no cards are still in players hands
   for(int ind = 0; ind < playersInDeal.size(); ind++){
      playersInDeal[ind]->clearHand();
   }

   playersInDeal.clear();

   deck = new Deck(true); // Create a new randomized deck
   comm = new CommunityCards(deck);
   
   sitOutNoChips(playersNowSittingOut); //Sit Out all players with no chips
   
   vector<int> activePlayers;
   playersAbleToPlay(activePlayers);

   if(moveButtonPostBlinds(activePlayers, randomizedButton)){                                   
      cout << players[buttonIndex].getName() << " is currently the button. missedBlind: " 
           << players[buttonIndex].missedBlind << ", isBlind: " << players[buttonIndex].isBlind << endl;
           
      for(int ind = 0; ind < players.size(); ind++){
         if(ind == buttonIndex){
           cout << players[ind].getName()   << " is currently the button. missedBlind: " 
                << players[ind].missedBlind << ", isBlind: " << players[ind].isBlind << endl;
         }
         else if(players[ind].missedBlind != NONE || players[ind].isBlind != NONE){
            cout << players[ind].getName()   << " missedBlind: " 
                 << players[ind].missedBlind << ", isBlind: " << players[ind].isBlind << endl;
         }
      }
      cout << "buttonIndex is " << buttonIndex << endl; 
      //system("PAUSE");
      
      if(headsUp){ //Just deal to button and big blind
         dealHandToPlayer(&players[buttonIndex]);
         for(int ind = buttonIndex + 1; ; ind++){
            if(ind == players.size()){
               ind = 0;
            }     
            
            if(ind == buttonIndex){
               cout << "Error occured in if(headsUp){" << endl;
               system("PAUSE");
            }
                 
            if(players[ind].isBlind == BIG_BLIND){
               dealHandToPlayer(&players[ind]);
               break;
            }
         }
      }
      else{
         int btn = 0;
         int bigBlindsDealt = (twoBigBlinds) ? 2 : 1;
      
         //cout << "bigBlindsDealt is " << bigBlindsDealt << endl;
         //system("PAUSE");
      
         for( ; activePlayers[btn] != buttonIndex; btn++){}
      
         for(int ind = btn + 1; ; ind++){ 
            if(ind == activePlayers.size()){
               ind = 0;
            }     
         
            //cout << "ind is now " << ind << endl;
            //system("PAUSE");
              
            if(bigBlindsDealt > 0){
               if(players[activePlayers[ind]].isBlind != BIG_BLIND && players[activePlayers[ind]].isBlind != SMALL_BLIND){
                  continue;
               }
               else if(players[activePlayers[ind]].isBlind == BIG_BLIND){
                  bigBlindsDealt--;
               }
            }
         
            dealHandToPlayer(&players[activePlayers[ind]]);       
         
            //All cards have been dealt
            if(ind == btn){
               break;
            }
         }
      }

      return true;
   }
   else{
      playersInDeal.clear(); 
      return false;
   }   
}

void PokerTable::clearDeck(){
   if(deck != NULL){
      delete deck;
      
      deck = NULL;
   }
}

void PokerTable::clearCommunity(){
   if(comm != NULL){
      delete deck;
      
      comm = NULL;
   }
}

//Can pass in a vector of <int>
//Function push_backs all active players indexes into this vector
int PokerTable::totalActivePlayers(vector<int> *activePlayers){
   if(activePlayers != NULL) activePlayers->clear();
    
   if(numberOfPlayers == 0){
      return 0;
   }
   else{
      int count = 0;  
        
      for(int ind = 0; ind < seats; ind++){
         if(players[ind].getName() != "" && !players[ind].sitout){
            count++;
            
            if(activePlayers != NULL) activePlayers->push_back(ind);
         }
      }
      
      return count;
   }
}

void PokerTable::playersAbleToPlay(vector<int> &activePlayers){
   activePlayers.clear();
   
   for(int ind = 0; ind < players.size(); ind++){
      if(players[ind].canStartHand()){
         activePlayers.push_back(ind);
      }
   }
}

void PokerTable::announceHoleCards(ICC* icc, int indent){
   for(int i = 0; i < playersInDeal.size(); i++){
      if(playersInDeal[i]->getName().compare("") != 0){
         stringstream ss;
         
         ss << "Your hand: " << playersInDeal[i]->getHand().toString();                                       
                                                   
         icc->tell(playersInDeal[i]->getName(), ss.str());
         
         vector<string> graphicHand;
         
         playersInDeal[i]->getHand().toStringGraphic(graphicHand, indent);
         
         for(int j = 0; j < graphicHand.size(); j++){
            icc->qtell(playersInDeal[i]->getName(), graphicHand[j]);
         }
         
         icc->qtell(playersInDeal[i]->getName(), " ");
      }
   }
}

bool PokerTable::announceCommunityCards(ICC* icc){
   if(comm != NULL){
      stringstream ss;
      
      ss << "Board: " << comm->toString(); 
           
      icc->tell(MY_CHANNEL, ss.str());
      return false;
   }
   else{
      return true;
   }
}

int PokerTable::newDeal(vector<SeatedPlayer*> *playersNowSittingOut, bool *randomizedButton){
   if(dealOver){
      //TEMPORARY
      //return all chips back to owners hand          
      for(int ind = 0; ind < players.size(); ind++){
         players[ind].addChips(players[ind].collectBet());
      }          
                          
      if(shuffleUpAndDeal(playersNowSittingOut, randomizedButton)){
         dealOver = false;
         return 0;
      }
      else{
         return TOO_FEW_PLAYERS;
      }
   }
   else{
      return DEAL_IN_PROGRESS;
   }
}

int PokerTable::join(string name, SeatedPlayer **player){
     return join(name, 1000, player);
}

SeatedPlayer* PokerTable::getPlayer(string name){
   SeatedPlayer* player = NULL;
   
   for(int ind = 0; ind < seats; ind++){
      if(players[ind].getName().compare(name) == 0){
         player = &players[ind];
      }
   }
   
   return player;
}

bool PokerTable::remove(const string &name){
   for(int ind = 0; ind < seats; ind++){
      if(players[ind].getName().compare(name) == 0){
         players[ind] = SeatedPlayer();                                     
                                              
         numberOfPlayers--;
         return true;
      }
   }
   
   return false;
}
   
int PokerTable::join(string name, ullong chips, SeatedPlayer **player){
     if(numberOfPlayers != seats){
        //Double check player is not already at table
        for(int ind = 0; ind < seats; ind++){
           if(players[ind].getName().compare(name) == 0){
              if(player != NULL){
                 (*player) = &players[ind];
              }
              return PLAYER_ALREADY_SEATED;
           }
        }                          
                        
        for(int ind = 0; ind < seats; ind++){
           if(players[ind].getName() == ""){
              players[ind] = SeatedPlayer(name, chips);
              
              if(player != NULL){
                 (*player) = &players[ind];
              }
              
              numberOfPlayers++;
              break;
           }
        }
        
        return 0;
     }
     //All seats have been taken
     else{
        return TABLE_FULL;
     }
}

#define PRINT_HEIGHT 13
#define ABOVE_NAME 1
#define ABOVE_CHIPS 2
#define ABOVE_CARDS 3
#define ABOVE_BET 4
#define ABOVE_EDGE_PLAYER_NAME 5
#define ABOVE_EDGE_PLAYER_CARDS 6
#define COMMUNITY_CARDS 7
#define BELOW_EDGE_PLAYER_NAME 8
#define BELOW_EDGE_PLAYER_CARDS 9
#define BELOW_BET 10
#define BELOW_CARDS 11
#define BELOW_CHIPS 12
#define BELOW_NAME 13

void PokerTable::print(vector<string> &text, const string &viewer){
   text.clear();
   
   vector<int> aboveIndex;
   vector<int> belowIndex;
   vector<int> aboveEdgeIndex;
   vector<int> belowEdgeIndex;
   
   aboveIndex.push_back(0); aboveIndex.push_back(1); aboveIndex.push_back(2);
   belowIndex.push_back(7); belowIndex.push_back(6); belowIndex.push_back(5); 
   aboveEdgeIndex.push_back(9); aboveEdgeIndex.push_back(3);
   belowEdgeIndex.push_back(8); belowEdgeIndex.push_back(4);
   
   for(int count = 1; count <= PRINT_HEIGHT; count++){
      switch(count){
         case ABOVE_NAME:
         case ABOVE_CHIPS:
         case ABOVE_CARDS:
         case ABOVE_BET:
            printAboveOrBelow(text, aboveIndex, count, viewer);
            break;
         case BELOW_BET:
         case BELOW_CARDS:
         case BELOW_CHIPS:
         case BELOW_NAME:
            printAboveOrBelow(text, belowIndex, count, viewer);
            break;
         case COMMUNITY_CARDS:
            printCommunity(text);
            break;
         case ABOVE_EDGE_PLAYER_NAME:
            printLeftAndRightEdge(text, aboveEdgeIndex, false, true, viewer);
            break;
         case ABOVE_EDGE_PLAYER_CARDS:
            printLeftAndRightEdge(text, aboveEdgeIndex, false, false, viewer);
            break;
         case BELOW_EDGE_PLAYER_NAME:
            printLeftAndRightEdge(text, belowEdgeIndex, true, true, viewer);
            break;
         case BELOW_EDGE_PLAYER_CARDS:
            printLeftAndRightEdge(text, belowEdgeIndex, false, false, viewer);
            break;
      }
   }
}

void PokerTable::printCommunity(vector<string> &text){
   stringstream ss;
   ss.str("");   
     
   for(int count = 1; count <= USERNAME_MAXIMUM + BUTTON_SIZE; count++) ss << " ";
   
   ss << "|";
   
   for(int count = 1; count <= LEFT_INDENT + USERNAME_MAXIMUM + BUTTON_SIZE; count++) ss << " ";
   
   string community;
   
   if(comm != NULL){
      community = comm->toString();
   }
   else{
      community = "";
   }
   
   ss << community;
   
   int size = community.size();
   
   for(int count = 1; count <= 2*USERNAME_MAXIMUM + 2 + 2*BUTTON_SIZE - size; count++) ss << " ";
   
   ss << "|";
   
   text.push_back(ss.str());
}

const int SPACESIZE = USERNAME_MAXIMUM + BUTTON_SIZE + 1;

void PokerTable::printLeftAndRightEdge(vector<string> &text, vector<int> &playerIndex, bool printPot, bool printName, const string &viewer){
   string name1 = string(players[playerIndex[0]].getName());
   string name2 = string(players[playerIndex[1]].getName());
   
   string chips1 = "";
   string chips2 = "";
   
   stringstream ss;
   ss.str("");
   stringstream ss2;
   ss2.str("");
   
   int size;
   
   //Print name + total chips left
   if(printName){
      if(name1 == ""){
         name1 = "<empty seat>";
      }
      else{
         ss2.str("");
         ss2 << "(";
         
         if(players[playerIndex[0]].isAllIn()){
            ss2 << "ALL-IN";
         }
         else{
            ss2 << players[playerIndex[0]].totalChips();
         }      
            
         ss2 << ")";   
         
         chips1 = string(ss2.str());
      }
      if(name2 == ""){
         name2 = "<empty seat>";
      }
      else{
         ss2.str("");
         ss2 << "(";
         
         if(players[playerIndex[1]].isAllIn()){
            ss2 << "ALL-IN";
         }
         else{
            ss2 << players[playerIndex[1]].totalChips();
         }      
            
         ss2 << ")";   
         
         chips2 = string(ss2.str());
      }          

      size = SPACESIZE - name1.size() - 1; //No space padding after user name
      
      for(int count = 1; count <= size; count++) ss << " ";
      
      ss << name1 << "|";
      
      if(name1 != "<empty seat>"){
         ss << chips1;
      }

      if(printPot){
         size = LEFT_INDENT + SPACESIZE - 1 - chips1.size();
         
         for(int count = 1; count <= size; count++) ss << " ";
         
         ss2.str("");
         ss2 << "Pot: " << pot;
         
         ss << ss2.str();
         
         size = 2 * SPACESIZE - ss2.str().size() - chips2.size(); 
      }
      else{
         size = LEFT_INDENT - 1 - chips1.size() + 3 * SPACESIZE - chips2.size();
      }
      
      for(int count = 1; count <= size; count++) ss << " ";
      
      ss << chips2 << "|" << name2;
      
      text.push_back(ss.str());
   }
   //Print Cards, Button, + Bet
   else{
      
      size = SPACESIZE - 1;
      for(int count = 1; count <= size; count++) ss << " ";
      
      ss << "|";
      
      if(!players[playerIndex[0]].isFolded()){ 
         if(players[playerIndex[0]].getName() == viewer || dealOver){
            ss2 << players[playerIndex[0]].getHand().toString();
         }
         else{
            ss2 << "[X,X]";
         }
      }
      
      if(buttonIndex == playerIndex[0]){
         ss2 << "(D)";
      }
      
      if(players[playerIndex[0]].totalBet() != 0){
         ss2 << "$" << players[playerIndex[0]].totalBet();
      }
      
      size = LEFT_INDENT - 1 + 3 * SPACESIZE - ss2.str().size();
      
      ss << ss2.str();
      
      ss2.str("");
      
      if(players[playerIndex[1]].totalBet() != 0){
         ss2 << "$" << players[playerIndex[1]].totalBet();
      }      
      
      if(buttonIndex == playerIndex[1]){
         ss2 << "(D)";
      }
      
      if(!players[playerIndex[1]].isFolded()){ 
         if(players[playerIndex[1]].getName() == viewer || dealOver){
            ss2 << players[playerIndex[1]].getHand().toString();
         }
         else{
            ss2 << "[X,X]";
         }
      }
      
      size -= ss2.str().size();
      
      for(int count = 1; count <= size; count++) ss << " ";
      
      ss << ss2.str() << "|";
      
      text.push_back(ss.str());
   }
}

void PokerTable::printAboveOrBelow(vector<string> &text, vector<int> &playerIndex, int type, const string &viewer){
   stringstream ss;
   ss.str("");
   
   int size;
   
   if((type == ABOVE_NAME) || (type == BELOW_NAME)){
      for(int count = 1; count <= USERNAME_MAXIMUM + BUTTON_SIZE; count++) ss << " ";
      
      for(int count = 1; count <= LEFT_INDENT; count++) ss << " ";
      
      for(int ind = 0; ind < playerIndex.size(); ind++){
         stringstream ss2;     
         string name = (players[playerIndex[ind]].getName() == "" ? "<empty seat>" : players[playerIndex[ind]].getName());
         string action = (playerIndex[ind] == actionIndex) ? "(!)" : "";
         
         ss2 << name << action;
         ss << ss2.str();
         
         size = ss2.str().size();
         size = SPACESIZE - size; //Extra + 1 is for padded space at end

         if(ind < playerIndex.size() - 1){
            for(int count = 1; count <= size; count++) ss << " ";
         }
      }
      
      text.push_back(ss.str());
   }
   else if((type == ABOVE_CHIPS) || (type == BELOW_CHIPS)){
      //(no space after user name)
      for(int count = 1; count <= SPACESIZE - 1; count++) ss << " ";
      
      ss << "+";
      
      for(int count = 1; count <= LEFT_INDENT - 1; count++) ss << "-";
      
      for(int ind = 0; ind < playerIndex.size(); ind++){
         stringstream ss2;
         
         if(players[playerIndex[ind]].getName() != ""){
            ss2 << "(";
         
            if(players[playerIndex[ind]].isAllIn()){
               ss2 << "ALL-IN";
            }
            else{
               ss2 << players[playerIndex[ind]].totalChips();
            }
         
            ss2 << ")";
         }
         
         ss << ss2.str();
         size = ss2.str().size();
         size = SPACESIZE - size;
         
         for(int count = 1; count <= size; count++) ss << "-";   
      }
      
      ss << "+";
      
      text.push_back(ss.str());
   }
   else if((type == ABOVE_CARDS) || (type == BELOW_CARDS)){
      for(int count = 1; count <= SPACESIZE - 1; count++) ss << " ";
      
      ss << "|";
      
      for(int count = 1; count <= LEFT_INDENT - 1; count++) ss << " ";
      
      for(int ind = 0; ind < playerIndex.size(); ind++){
         stringstream ss2;
         string cards;     
         
         if(!players[playerIndex[ind]].isFolded()){ 
            if(players[playerIndex[ind]].getName() == viewer || dealOver){
               cards = players[playerIndex[ind]].getHand().toString();
            }
            else{
               cards = "[X,X]";
            }
         }
         else{
            cards = "";
         }
         
         ss2 << cards;
         
         if(buttonIndex == playerIndex[ind]){
            ss2 << "(D)";
         }
         
         ss << ss2.str();
         
         size = SPACESIZE - ss2.str().size();
         
         for(int count = 1; count <= size; count++) ss << " ";
      }
      
      ss << "|";
      
      text.push_back(ss.str());
   }
   else if((type == ABOVE_BET) || (type == BELOW_BET)){
      cout << "In ABOVE_BET statement" << endl;
      
      for(int count = 1; count <= SPACESIZE - 1; count++) ss << " ";
      
      ss << "|";
      
      for(int count = 1; count <= LEFT_INDENT - 1; count++) ss << " ";
      
      for(int ind = 0; ind < playerIndex.size(); ind++){
         stringstream ss2;
         ss2.str("");

         if(players[playerIndex[ind]].totalBet() != 0){
            ss2 << "$" << players[playerIndex[ind]].totalBet();
         }
         
         ss << ss2.str();
         
         size = SPACESIZE - ss2.str().size();
         
         for(int count = 1; count <= size; count++) ss << " ";
      }
      
      ss << "|";
      
      cout << "ss is " << ss.str() << endl;
      
      text.push_back(ss.str());
   }
}
