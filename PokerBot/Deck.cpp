#include "Deck.h"
#include <iostream>
//#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h> 

#define RAND_SHUFFLE_KEY 391   //This adds a layer of security to the start seed

Deck::Deck(bool random){
   vector<Card> * allCards = new vector<Card>();
   
   for(int suit = 0; suit <= 3; suit++){ //there are 4 suits
      for(int rank = ACE; rank <= KING; rank++){
         allCards->push_back(Card(rank, suit));
      }
   }
   
   if(random){
      srand(time(NULL));  
      int ind;
      
      //ensures it to be much harder to determine the calculate the next pseudo-random number
      //If the user knows that the seed is based on the time.
      //The user will also need to know that RAND_SHUFFLE_KEY calls were made to rand()
      //function before creation of the deck
      for(int count = 1; count <= RAND_SHUFFLE_KEY; count++) rand(); 
              
      deck = new vector<Card>();   
      
      while(allCards->size() > 0){
         ind = rand() % allCards->size();
         
         deck->push_back((*allCards)[ind]);
         
         allCards->erase(allCards->begin() + ind);
      }        
              
      delete allCards;
   }
   else{
      deck = allCards;
   }
}

void Deck::print(){
   if(deck->size() > 0){
      for(int ind = 0; ind < deck->size() - 1; ind++){
         cout << (*deck)[ind].toString() << " " << endl;
      }
   
      cout << (*deck)[deck->size() - 1].toString() << endl;
   }
}

Card Deck::dealTopCard(){
     Card c(deck->back());
     
     deck->pop_back();
     
     return c;
}
