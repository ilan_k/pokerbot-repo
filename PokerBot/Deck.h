#ifndef	_DECK_H_
#define	_DECK_H_

#include <vector>
#include "Card.h"

//Deck contains 52 cards

using namespace std;

class Deck{
public:
   //intializes a deck of cards
   //random set to true creates a random deck
   Deck(bool random);
   
   ~Deck(){
      if(deck != NULL){
         delete deck;
      }
   }
   
   void print();
   
   int size(){
      return deck->size();
   }
   
   //Removes the top card off the deck and returns the removed card
   Card dealTopCard();
private:
   vector<Card> *deck;
};

#endif
