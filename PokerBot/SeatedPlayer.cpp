#include "SeatedPlayer.h"

#include <sstream>

SeatedPlayer::SeatedPlayer(){
   leave();
}

void SeatedPlayer::leave(){
     chips = 0;
     name = "";
     sitout = true; 
     folded = true; //has no hand
     bet = 0;
     allin = false;
     missedBlind = NONE;
     isBlind = NONE;
     newPlayer = true;
}

SeatedPlayer::SeatedPlayer(string n, ullong start, bool so, int missed, int blind, bool np) 
       : name(n), chips(start), folded(true), bet(0), allin(false), missedBlind(missed), isBlind(blind), newPlayer(np){ 
   if(chips == 0){
      sitout = true;
   }
   else{
      sitout = so;
   }
}

string SeatedPlayer::status(){
   stringstream status;    
       
   status << "Chips: " << chips << ", ";
   
   if(!folded){
      //Still has cards
      status << "Cards: " << hand.toString();
   }
   else{
      status << "and Sitting ";
   
      if(sitout){
         status << "Out";
      }
      else{
         status << "In";
      }
   }
   return status.str();
}
