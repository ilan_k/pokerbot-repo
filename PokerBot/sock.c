// Handle socket stuff.


#include <cstdio>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <cerrno>

//unix libraries
//#include <sys/socket.h>
//#include <netdb.h>
//#include <arpa/inet.h>
#include <iostream>
#include <winsock2.h>

using namespace std;

#include "sock.h"

extern unsigned GetTime();

// Construct.
TcpSocket::TcpSocket(const char *serverName, int serverPort, int localPort,
		bool debug) :
	sock(0), valid(false), tries(0), serverPort(serverPort), localPort(
			localPort), lastTransmit(0), debug(debug) {
	strcpy(this->serverName, serverName);
}

// Clean up.
TcpSocket::~TcpSocket() {
	if (valid)
		close();
}

// Is there data waiting?
bool TcpSocket::dataWaiting() {
	fd_set rfds;
	struct timeval tv;

	FD_ZERO(&rfds);
	FD_SET(sock, &rfds);

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	bool result = select(sock + 1, &rfds, NULL, NULL, &tv);
	return result;
}

// Read a line or a datagram from the input.  A line will always end with \n.
// A datagram will end with "\031)".
size_t TcpSocket::receive(char *buf) {
	if (!valid)
		return 0;

	if (!dataWaiting())
		return 0;

	int pos = 0;
	while (1) {
		if (recv(sock, buf + pos, 1, 0) <= 0) {
			fprintf(stderr, "receive error.\n");
			valid = 0;
			return 0;
		}
		pos++;

		if (buf[pos - 1] == ')' && buf[pos - 2] == '\031')
			break;
		if (buf[0] != '\031' && buf[pos - 1] == '\n')
			break;
	}

	buf[pos] = 0;

	return pos;
}

void TcpSocket::transmit_va(const char *fmt, va_list va_args) {
	if (!valid)
		return;

	char text[1024];
	vsprintf(text, fmt, va_args);

	int length = strlen(text);

	if (text[length - 1] != '\n' || text[length - 2] != '\r') {
		text[length] = '\r';
		text[length + 1] = '\n';
		text[length + 2] = 0;
		length += 2;
	}

	if (send(sock, text, length, 0) <= 0) {
		fprintf(stderr, "send error.\n");
		valid = 0;
		return;
	}

	if (debug) {
		printf("[send] %s", text);
		fflush( stdout);
	}
}

void TcpSocket::open() {
	tries++;
	fprintf(stderr, "Attempting to connect to %s port %d... (%d)\n",
			serverName, serverPort, tries);

	// If we think we are connected, close the old connection.
	if (valid)
		close();

	// Spawn a timestamp, then pause a second to allow it to get ready.
	char ps[8], lps[8];
	sprintf(ps, "%d", serverPort);
	sprintf(lps, "%d", localPort);
	//Comment out the next 6 lines if not using timestamp.
	//	int rv = spawnl(_P_NOWAIT, "timestamp", "timestamp", server, ps, "-p", lps, NULL);
	//	if (rv == -1)
	//	{
	//		fprintf(stderr, "spawnl error.\n");
	//		return;
	//	}
	//	Sleep(1000);

	// Do name lookup.
	//struct hostent *hostlookup = gethostbyname("localhost"); //for timestamp on
	cout << "h_errno is " << h_errno << endl;
	
	struct hostent *hostlookup = gethostbyname("chessclub.com"); //for timestamp off
	if (hostlookup == NULL) {
		fprintf(stderr, "gethostbyname error.\n");
        cout << "Could not connect to ICC" << endl;
        cout << "h_errno is " << h_errno << endl;
        system("PAUSE");
		return;
	}
	unsigned long host = ((struct in_addr *) hostlookup->h_addr)->s_addr;

	// Setup socket.
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		fprintf(stderr, "socket error.\n");
		return;
	}

	// Make connection.
	struct sockaddr_in in;
	memset(&in, 0, sizeof(struct sockaddr_in));
	in.sin_family = AF_INET;
	//in.sin_port = htons(local_port); //for timestamp on
	in.sin_port = htons(serverPort); //for timestamp off
	in.sin_addr.s_addr = host;
	if (connect(sock, (struct sockaddr *) &in, sizeof(struct sockaddr_in))
			== -1) {
		fprintf(stderr, "connect error.\n");
		return;
	}

	// Done.
	valid = 1;
}

// Close the connection.

void TcpSocket::close() {
	if (valid) {
		//shutdown(sock, SHUT_RDWR);
		shutdown(sock, SD_BOTH);
		valid = 0;
	}
}
