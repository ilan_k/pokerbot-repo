#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <ctype.h>

using namespace std;

string toLowerCase(const string &str);

#endif
