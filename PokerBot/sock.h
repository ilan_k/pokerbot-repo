#ifndef SOCK_H
#define SOCK_H

#include <cstdarg>

/** A socket to the ICC server. */
class TcpSocket {
	// THE socket.
	int sock;

	// Is the connection valid?
	bool valid;

	// How many connection attempts?
	int tries;

	// Connection details.
	char serverName[256];
	int serverPort;
	int localPort;

	// Time of the last send to ICC.
	unsigned lastTransmit;

	// Is there data waiting?
	bool dataWaiting();

	// Logging?
	bool debug;

public:

	// Constructor.
	TcpSocket(const char *serverName, int serverPort, int localPort = 5002,
			bool debug = false);

	// Clean up.
	~TcpSocket();

	// Do we have a valid connection?
	int isValid() const {
		return valid;
	}

	// Read a line or a datagram from the input.
	size_t receive(char *buf);

	// Send data to ICC.
	void transmit(const char *fmt, ...) {
		va_list va_args;
		va_start(va_args, fmt);
		transmit_va(fmt, va_args);
		va_end(va_args);
	}

	void transmit_va(const char *fmt, va_list va_args);

	// Open the connection.
	void open();

	// Close the connection.
	void close();

	// Return the idle time in seconds.
	unsigned idle() const {
		return 0;
	}
};

#endif
