#include "Hand.h"
#include <iostream>
#include <sstream>

using namespace std;

string Hand::toString(){
   if(hand.size() > 0){
      stringstream ss;
   
      ss << "[";
   
      for(int ind = 0; ind < hand.size() - 1; ind++){
         ss << hand[ind].toString() << ", ";
      }
   
      ss << hand[hand.size() - 1].toString() << "]";
   
      return ss.str();
   }
   else{
      return "[]";
   }
}

void Hand::toStringGraphic(vector<string> &text, int indent){
   vector< vector<string> > graphicalHand;
   
   for(int ind = 0; ind < hand.size(); ind++){
      vector<string> graphicCard;     
           
      hand[ind].toStringGraphic(graphicCard);

      graphicalHand.push_back(graphicCard);
   }
   
   string line;
   
   //For each line of every card in the hand, store it into text 
   for(int i = 0; i < (graphicalHand[0]).size(); i++){
      line = "";
      
      for(int j = 0; j < indent; j++){
         line.append(" ");
      }
      
      for(int j = 0; j < graphicalHand.size(); j++){
         line.append((graphicalHand[j])[i]);
         
         if(j < graphicalHand.size() - 1){
            line.append(" ");
         }
      }
      
      text.push_back(line);
   }
}

/*Hand& Hand::operator=(const Hand &other){
   for(int ind = 0; ind < other.hand.size(); ind++){
      this->hand.push_back(other.hand[ind]);
   }
}*/
