#ifndef	__ICC_H
#define	__ICC_H

#include <string>
#include <cstdarg>
#include <cstring>
#include "sock.h"
#include <stdio.h>

#define MY_CHANNEL 128
#define MY_MANAGER "Snuffleupagus"

using namespace std;

typedef int Channel;

class ICCListener {
public:
	virtual void onWhoAmI(const string &userName, const string &userTitles) = 0;
	virtual void onUserArrived(const string &userName) = 0;
	virtual void onUserDeparted(const string &userName) = 0;
	virtual void onTellReceived(const string &tellerName,
			const string &tellerTitles, const string &text) = 0;
	virtual ~ICCListener() {}
};

class ICC {

public:

	enum TellType {
		TELL = 0, XTELL, QTELL, ATELL, MESSAGE
	};

	ICC(const string& serverName, int serverPort, const string& loginName,
			const string& password, bool autoReconnect = true, bool debug =
					false) :
		autoReconnect(autoReconnect), debug(debug), serverName(serverName),
				serverPort(serverPort), loginName(loginName),
				password(password), userName(), socket(serverName.c_str(),
						serverPort), stored_type(GD_NONE) {
	}

	virtual ~ICC() {}

	const string &getServerName() const {
		return serverName;
	}

	const int getServerPort() const {
		return serverPort;
	}

	/**
	 * For guest and anon logins, this may not match the loginName.  In these
	 * cases, it may also change upon reconnection.
	 */
	const string &getUserName() const {
		return userName;
	}

	const bool getAutoReconnect() const {
		return autoReconnect;
	}

	const bool getDebug() const {
		return getDebug();
	}

	/**
	 * @returns true if the connection was successful.
	 */
	bool connect();

	/**
	 * Dispatches the next line of input to the listener.
	 * If the connection is closed, then this method returns immediately.
	 * If the connection is open, this method will block unti data is available.
	 */
	void processNextInput(ICCListener &listener);

	void setVar(const string &variable, const int value) {
		sendCommand("set %s %d", variable.c_str(), value);
	}

	void setVar(const string &variable, const string &value) {
		sendCommand("set %s %s", variable.c_str(), value.c_str());
	}

	void away(const char *msg, ...) {
		va_list va_args;
		va_start(va_args, msg);
		away_va(msg, va_args);
		va_end(va_args);
	}

	void away_va(const char *msg, va_list va_args) {
		char text[1024];
		vsprintf(text, msg, va_args);
		sendCommand("away %s", text);
	}

	void busy(const int busy = 2) {
		setVar("busy", busy);
	}

	void busy(const int busy, const char *msg, ...) {
		va_list va_args;
		va_start(va_args, msg);
		busy_va(busy, msg, va_args);
		va_end(va_args);
	}

	void busy_va(const int busy, const char *msg, va_list va_args) {
		char text[1024];
		vsprintf(text, msg, va_args);
		sendCommand("multi set busy %d; away %s", busy, msg);
	}

	void clearBusy() {
		busy(0);
	}

	void addNotify(const string &userName) {
		sendCommand("+notify %s", userName.c_str());
	}

	void removeNotify(const string &userName) {
		sendCommand("-notify %s", userName.c_str());
	}

	void addChannel(const Channel channel) {
		sendCommand("+channel %d", channel);
	}

	void addChannel(const string &userName, const Channel channel) {
		sendCommand("qchanplus %s %d", userName.c_str(), channel);
	}

	void removeChannel(const Channel channel) {
		sendCommand("-channel %d", channel);
	}

	void removeChannel(const string &userName, const Channel channel) {
		sendCommand("qchanminus %s %d", userName.c_str(), channel);
	}

	void censor(const string &username) {
		sendCommand("+censor %s", username.c_str());
	}

	void uncensor(const string &username) {
		sendCommand("-censor %s", username.c_str());
	}

	/**
	 * Send a message to the designated channel.
	 *
	 * @param command The command to use when sending the message to the user.
	 * Example values include "tell", "xtell", "qtell", "message", "qchanplus",
	 * "suggest", or anything else which takes a userName as the very next
	 * argument.
	 */
	void sendTo(const string &userName, const TellType type, const string &text) {
		sendTo(userName, type, text.c_str());
	}

	void sendTo(Channel channel, const TellType type, const string &text) {
		sendTo(channel, type, text.c_str());
	}

	void sendTo(const string &userName, const TellType type,
			const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		sendTo_va(userName, type, format, va_args);
		va_end(va_args);
	}

	void sendTo(Channel channel, const TellType type,
			const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		sendTo_va(channel, type, format, va_args);
		va_end(va_args);
	}

	void sendTo_va(const string &userName, const TellType type,
			const char *format, va_list va_args) {
		char text[1024];
		vsprintf(text, format, va_args);
		sendToRaw(userName, type, text);
	}

	void sendTo_va(Channel channel, const TellType type,
			const char *format, va_list va_args) {
		char text[1024];
		vsprintf(text, format, va_args);
		sendToRaw(channel, type, text);
	}


	void tell(const string &userName, const string &text) {
		sendTo(userName, TELL, text);
	}

	void tell(const Channel channel, const string &text) {
		sendTo(channel, TELL, text);
	}

	void tell(const string &userName, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		tell_va(userName, format, va_args);
		va_end(va_args);
	}

	void tell(const Channel channel, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		tell_va(channel, format, va_args);
		va_end(va_args);
	}

	void tell_va(const string &userName, const char *format, va_list va_args) {
		sendTo_va(userName, TELL, format, va_args);
	}

	void tell_va(const Channel channel, const char *format, va_list va_args) {
		sendTo_va(channel, TELL, format, va_args);
	}


	void xtell(const string &userName, const string &text) {
		sendTo(userName, XTELL, text);
	}

	void xtell(const Channel channel, const string &text) {
		sendTo(channel, XTELL, text);
	}

	void xtell(const string &userName, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		xtell_va(userName, format, va_args);
		va_end(va_args);
	}

	void xtell(const Channel channel, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		xtell_va(channel, format, va_args);
		va_end(va_args);
	}

	void xtell_va(const string &userName, const char *format, va_list va_args) {
		sendTo_va(userName, XTELL, format, va_args);
	}

	void xtell_va(const Channel channel, const char *format, va_list va_args) {
		sendTo_va(channel, XTELL, format, va_args);
	}


	void qtell(const string &userName, const string &text) {
		sendTo(userName, QTELL, text);
	}

	void qtell(const Channel channel, const string &text) {
		sendTo(channel, QTELL, text);
	}

	void qtell(const string &userName, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		qtell_va(userName, format, va_args);
		va_end(va_args);
	}

	void qtell(const Channel channel, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		qtell_va(channel, format, va_args);
		va_end(va_args);
	}

	void qtell_va(const string &userName, const char *format, va_list va_args) {
		sendTo_va(userName, QTELL, format, va_args);
	}

	void qtell_va(const Channel channel, const char *format, va_list va_args) {
		sendTo_va(channel, QTELL, format, va_args);
	}


	void atell(const string &userName, const string &text) {
		sendTo(userName, ATELL, text);
	}

	void atell(const Channel channel, const string &text) {
		sendTo(channel, ATELL, text);
	}

	void atell(const string &userName, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		atell_va(userName, format, va_args);
		va_end(va_args);
	}

	void atell(const Channel channel, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		atell_va(channel, format, va_args);
		va_end(va_args);
	}

	void atell_va(const string &userName, const char *format, va_list va_args) {
		sendTo_va(userName, ATELL, format, va_args);
	}

	void atell_va(const Channel channel, const char *format, va_list va_args) {
		sendTo_va(channel, ATELL, format, va_args);
	}


	void message(const string &userName, const string &text) {
		sendTo(userName, MESSAGE, text);
	}

	void message(const Channel channel, const string &text) {
		sendTo(channel, MESSAGE, text);
	}

	void message(const string &userName, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		message_va(userName, format, va_args);
		va_end(va_args);
	}

	void message(const Channel channel, const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		message_va(channel, format, va_args);
		va_end(va_args);
	}

	void message_va(const string &userName, const char *format, va_list va_args) {
		sendTo_va(userName, MESSAGE, format, va_args);
	}

	void message_va(const Channel channel, const char *format, va_list va_args) {
		sendTo_va(channel, MESSAGE, format, va_args);
	}


	void sendUnknownCommand(const string &command) {
		sendCommand(command);
	}

	void sendUnknownCommand(const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		sendUnknownCommand_va(format, va_args);
		va_end(va_args);
	}

	void sendUnknownCommand_va(const char *format, va_list va_args) {
		sendCommand_va(format, va_args);
	}

private:

	static const string tellTypeStrings[];
	enum GD {
		GD_NONE, GD_DATAGRAM, GD_LINE
	};

	const bool autoReconnect;
	const bool debug;
	const string serverName;
	const int serverPort;
	const string loginName;
	const string password;
	string userName; //guests & anon players have a dynamically assigned name.
	TcpSocket socket;

	/** Sometimes we must store data from ICC for later processing. */
	char stored_data[10000];
	GD stored_type;

	void sendCommand(const string &command);
	void sendCommand_va(const char *format, va_list va_args);

	void sendCommand(const char *format, ...) {
		va_list va_args;
		va_start(va_args, format);
		sendCommand_va(format, va_args);
		va_end(va_args);
	}

	virtual void parseDatagram(ICCListener &listener, char *buf);
	virtual void parseLine(ICCListener &listener, char *buf);

	void sendToRaw(const string &userName, const TellType type,
			const char *text) {
		const string &command = tellTypeStrings[type];
		if (strlen(text) == 0) {
			sendCommand("%s %s!", command.c_str(), userName.c_str());
		} else if (text[0] != ' ') {
			sendCommand("%s %s! %s", command.c_str(), userName.c_str(), text);
		} else {
			sendCommand("%s %s! %s", command.c_str(), userName.c_str(), text);
		}
	}

	void sendToRaw(const Channel channel, const TellType type, const char *text) {
		const string &command = tellTypeStrings[type];
		if (strlen(text) == 0) {
			sendCommand("%s %d!", command.c_str(), channel);
		} else if (text[0] != ' ') {
			sendCommand("%s %d! %s", command.c_str(), channel, text);
		} else {
			sendCommand("%s %d! %s", command.c_str(), channel, text);
		}
	}

};

#endif
