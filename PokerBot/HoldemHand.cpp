#include "HoldemHand.h"
#include "pokerlib.h"

unsigned int HoldemHand::assess(CommunityCards * comm){
   if(hand.size() == 2 && comm->getCommunity().size() == 5){
      int array[7];            
                  
      array[0] = hand[0].convertCardToBitNumber();
      array[1] = hand[1].convertCardToBitNumber();
   
      for(int ind = 2; ind < 7; ind++){
         array[ind] = comm->getCommunity()[ind - 2].convertCardToBitNumber();
      }
   
      return eval_7hand( array );
   }
   else{
      return 0;
   }
}
