// Main ICC interface.

#include <cctype>
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

#include <stdio.h>

using namespace std;

#include "icc.h"
//#include "data.h"
#include "datagram.h"
//#include "defs.h"
//#include "print.h"
#include "sock.h"

#include <Windows.h>

static inline int smatch(const char *s1, const char *s2) {
	return strncmp(s1, s2, strlen(s2)) == 0;
}

const string ICC::tellTypeStrings[] = { "tell", "xtell", "qtell", "atell",
		"message" };

/**
 * Send the given line to the server.
 */
void ICC::sendCommand_va(const char *format, va_list va_args) {
	socket.transmit_va(format, va_args);
}

void ICC::sendCommand(const string &command) {
	socket.transmit("%s", command.c_str());
}


// Make a connection to ICC.
bool ICC::connect() {
	socket.open();
	if (!socket.isValid()){
		return false;
    }

	// Turn on datagrams.
	char dgs[] =
	"0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

	dgs[DG_WHO_AM_I] = '1';
	dgs[DG_PERSONAL_TELL] = '1';
	dgs[DG_NOTIFY_ARRIVED] = '1';
	dgs[DG_NOTIFY_LEFT] = '1';

	sendCommand("level2settings=%s", dgs);

	// Send login info.
	sendCommand(loginName);
	sendCommand(password);

	// Other things to initialize.
	censor("ROBOadmin");
	setVar("shout", 1);
	setVar("prompt", 0);
	setVar("bell", 0);
	setVar("style", 13);
	setVar("noautologout", 1);
	if (debug) {
		setVar("busy", 2);
	}
	return true;
}

void ICC::parseDatagram(ICCListener &listener, char *buf) {
	Datagram dg(buf);

	// in debug mode, write the datagram to stdout
	if (debug) {
		cout << "Received datagram of type " << dg.Type() << ", data:" << endl;
		for (int i = 0; i < dg.ArgC(); i++) {
			cout << "[" << dg.Arg(i) << "] ";
		}
		cout << endl;
	}

	// handle different datagrams differently
	switch (dg.Type()) {
	case DG_PERSONAL_TELL: {
		string tellerName = dg.Arg(0);
		string tellerTitles = dg.Arg(1);
		string text = dg.Arg(2);
		listener.onTellReceived(tellerName, tellerTitles, text);
		/*if (debug) {
			tell(MY_CHANNEL, "Received tell: [%s %s]", dg.Arg(0), dg.Arg(2));
		}*/
		break;
	}
	case DG_NOTIFY_ARRIVED: {
		string userName = dg.Arg(0);
		listener.onUserArrived(userName);
		break;
	}
	case DG_NOTIFY_LEFT: {
		string userName = dg.Arg(0);
		listener.onUserDeparted(userName);
		break;
	}
	case DG_WHO_AM_I: {
		string userName = dg.Arg(0);
		string userTitles = dg.Arg(1);
		this->userName = userName + userTitles;
		listener.onWhoAmI(userName, userTitles);
		break;
	}
	default: {
		break;
	}
	}
}

// Parse a line of text from ICC.
void ICC::parseLine(ICCListener &listener, char *buf) {
	// Skip blank lines.
	if (strlen(buf) == 2)
		return;

	// Stuff to flush.
	if (smatch(buf, "   **ANNOUNCEMENT**") || smatch(buf, "Your ad is #")
			|| smatch(buf, "(ad sent to") || smatch(buf, "\"Seeking\" ad #")
			|| smatch(buf, "You accept the challenge of") || smatch(buf,
			"Creating: ") || smatch(buf, "You may accept this with") || smatch(
			buf, "\"decline ") || smatch(buf, "(told ") || smatch(buf,
			"(whispered ") || smatch(buf, "It isn't your turn") || // For krieg!
			smatch(buf, "(kibitzed "))
		return;

	// Just print it.
	// printf("#%s", buf);
}

// Obtain and process a piece of data from ICC.
void ICC::processNextInput(ICCListener &listener) {
	// error?
	if (!socket.isValid()) {
		if (autoReconnect) {
			// Close the socket.  Needed?  Fixes problems?  :)
			//			SockClose();

			stored_type = GD_NONE;
			Sleep(10);
			//			Sleep(10000);
			fprintf(stderr, "*** attempting to reconnect...\n");
			connect();
			return;
		}
		exit(1);
	}

	// Get data to process if we don't already have some.
	if (stored_type == GD_NONE) {
		size_t pos = socket.receive(stored_data);
		if (pos == 0)
			stored_type = GD_NONE;
		else if (stored_data[pos - 1] == '\n')
			stored_type = GD_LINE;
		else
			stored_type = GD_DATAGRAM;
	}

	// no data available?
	if (stored_type == GD_NONE) {
		Sleep(100);
		//Sleep(10);
		return;
	}

	if (stored_type == GD_LINE) {
		// parse line
		stored_type = GD_NONE;
		parseLine(listener, stored_data);
	} else {
		// parse datagram
		stored_type = GD_NONE;
		parseDatagram(listener, stored_data);
	}
}

