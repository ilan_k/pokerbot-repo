#include "Card.h"
#include "arrays.h"
#include <iostream>
#include <sstream>

void Card::appendRank(string &text){
   if(rank == ACE){
      text.append("A");
   }
   else if(rank == KING){
      text.append("K");
   }
   else if(rank == QUEEN){
      text.append("Q");
   }
   else if(rank == JACK){
      text.append("J");
   }
   else if(rank == TEN){
      text.append("T");
   }
   else if(rank >= TWO && rank <= NINE){
      char r = rank + '0';
      text += r;
   }
   //should not happen
   else{
      text.append("X");
      cout << "ERROR: Invalid rank given" << endl;
      //system("PAUSE");
   }  
}

string Card::toString(){
   string card = "";
   
   appendRank(card);
   
   switch(suit){
      case CLUBS:
           card += 'c';
           break;
      case DIAMONDS:
           card += 'd';
           break;
      case HEARTS:
           card += 'h';
           break;
      case SPADES:
           card += 's';
           break;
      //should not happen
      default:
           cout << "ERROR: Invalid suit given" << endl;
           //system("PAUSE");
   }
   
   return card;
}


//+--------+--------+--------+--------+
//|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
//+--------+--------+--------+--------+

unsigned int Card::convertCardToBitNumber(){         
   int r = (rank == ACE) ? 12 : rank - TWO;
   int s = 0x8000 >> suit;    
    
   unsigned int bitNumber = primes[r] | (r << 8) | s | (1 << (16 + r));
   
   return bitNumber;
}

void Card::toStringGraphic(vector<string> &text){
   if(suit == CLUBS || suit == DIAMONDS || suit == HEARTS || suit == SPADES){
      text.push_back(".------.");
              
      string rankline1 = "|";
      appendRank(rankline1);
      string rankline2;  
     
      switch(suit){
         case CLUBS:
            //cout << "Clubs" << endl;
            rankline1.append(" _   |");
            text.push_back(rankline1);
            text.push_back("| ( )  |");
            text.push_back("|(_._) |");
            rankline2 = "|  Y  ";  
            appendRank(rankline2);
            rankline2.append("|");
            text.push_back(rankline2);
            break;
         case DIAMONDS:
            //cout << "Diamonds" << endl;
            rankline1.append(" /\\  |");
            text.push_back(rankline1);
            text.push_back("| /  \\ |");
            text.push_back("| \\  / |");
            rankline2 = "|  \\/ ";
            appendRank(rankline2);
            rankline2.append("|");
            text.push_back(rankline2);
            break;
         case HEARTS:
            //cout << "Hearts" << endl;
            rankline1.append("_  _ |");
            text.push_back(rankline1);
            text.push_back("|( \\/ )|");
            text.push_back("| \\  / |");
            rankline2 = "|  \\/ ";
            appendRank(rankline2);
            rankline2.append("|");
            text.push_back(rankline2);
            break;
         case SPADES:
            //cout << "Spades" << endl;
            rankline1.append(" .   |");
            text.push_back(rankline1);
            text.push_back("| / \\  |");
            text.push_back("|(_,_) |");
            rankline2 = "|  I  ";
            appendRank(rankline2);
            rankline2.append("|");
            text.push_back(rankline2);
            break;
      }
   
      text.push_back("`-----+\'");
   }
}
