#ifndef _ICCPOKERBOT_H_
#define _ICCPOKERBOT_H_

#include <iomanip>
#include <locale>
#include <iostream>
#include <vector>
#include <sstream>
#include "utils.h"

#include "icc.h"
#include "PokerTable.h"
#include "pokerlib.h"

class ICCPokerBot{
public:
   ICCPokerBot(string sn, string u, string password);
   
   ICCPokerBot(string password);
   
   int run();
   
   ~ICCPokerBot(){
      closeConnection();
      
      closeTable();
   }
   
   string username;
   ICC *icc;

   void tellInvalid(const string &teller);
   void ParseTell(const string &tellerName, const string &tellerTitles, const string &text);

private:
   ICCPokerBot(); //disallow default constructor, must have password specified
   void closeConnection();
   void closeTable();
   bool connect();   
   void loadTable();
   void joinTable(const string &teller);
   void statusRequested(const string &teller);
   void helpRequested(const string &teller);
   void quitTable(const string &teller);
   int newDeal(const string &teller);
   void notReady(const string &teller);  
   void sitoutRequested(const string &teller);   
   void sitinRequested(const string &teller); 
   string playersAtTable();
   void dealFlopRequested(const string &teller); 
   void dealTurnRequested(const string &teller); 
   void dealRiverRequested(const string &teller); 
   void showDownRequested(const string &teller);
   void tell(string user, string mess, bool qtell = false, bool includeBotName = false);
   void tell(Channel channel, string mess, bool qtell = false, bool includeBotName = false);
   void tellTable(string mess, bool qtell = false, bool includeBotName = false);
  
   void displayTable(const string &teller);
   void displayTableAll(const string &teller);    
        
   bool loadProfiles(); 
   void saveProfiles();
   bool isRegistered(const string &teller);   
   
   void fullDeal(const string &teller); 
        
   string servername;
   string password;
   bool connected;
   bool running;
   
   PokerTable * table;
   vector< pair<string, ullong> > profiles;
   
   //PokerBotListener listener;
   
   class PokerBotListener: public ICCListener {
   public:
       void setBot(ICCPokerBot *b){ bot = b; }
       
	   void onWhoAmI(const string &userName, const string &userTitles){
       }
    
   	   void onUserArrived(const string &userName){
       }
    
	   void onUserDeparted(const string &userName){
       }
    
       void onTellReceived(const string &tellerName, const string &tellerTitles, const string &text){
          if(bot != NULL){
             bot->ParseTell(tellerName, tellerTitles, text);
          }
       }
    
   private:
      ICCPokerBot *bot;
   }listener;
};

#endif
