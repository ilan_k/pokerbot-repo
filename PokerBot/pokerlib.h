#ifndef _POKERLIB_H_
#define _POKERLIB_H_

#include <string>

#include "poker.h"

#define ALL_POKER_HANDS_FILE "PokerHands.txt"

using namespace std;

void init_deck( int *deck );

int hand_rank( short val );

short eval_5hand( int *hand );

short eval_7hand( int *hand );

string rankToString(int rank);

//Returns back description of this equivalence class
string rankToString(short equivalenceClass, string filename);

#endif
