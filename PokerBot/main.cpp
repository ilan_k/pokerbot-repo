#include <cstdlib>
#include <iostream>

#include "ICCPokerBot.h"

//Used for testing
#include "Deck.h"
#include "HoldemHand.h"
#include "CommunityCards.h"
#include "PokerTable.h"
#include "pokerlib.h"
#include "arrays.h"
#include <iostream>
#include <iomanip>
#include <locale>
#include <string>

#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "ws2_32")

#include <stdio.h>

#include <sstream>

using namespace std;

#define WIDTH 30

string uintToString(unsigned int n){
    std::stringstream ss;
    ss.str("");
    ss << n;
    std::string s(ss.str());
    return s;
}

string intToString(int n){
    std::stringstream ss;
    ss.str("");
    ss << n;
    std::string s(ss.str());
    return s;
}

void display(string name, string cards){
   cout << name << ":";
   cout << setw(WIDTH - name.size()) << cards << endl;
}

void tester(){
    /*Deck d1(true);
    d1.print();
    cout << "Size of d1 is " << d1.size() << endl;
    system("PAUSE");
    
    Card c1 = d1.dealTopCard();
    Card c2 = d1.dealTopCard();
    
    HoldemHand h(c1, c2);
    
    cout << "Card dealt is " << c1.toString() << endl;
    cout << "Card dealt is " << c2.toString() << endl;
    system("PAUSE");
    cout << h.toString() << endl;
    system("PAUSE");
    d1.print();
    system("PAUSE");
    
    CommunityCards com(&d1);
    
    com.dealTurn(); // should be error
    com.dealRiver(); // should be error
    
    system("PAUSE");
    com.dealFlop();
    cout << "Flop: " << com.flopString() << endl;
    
    system("PAUSE");
    com.dealFlop(); // should be error
    system("PAUSE");
    
    com.dealTurn();
    cout << "Turn: " << com.turnString() << endl;
    system("PAUSE");
    com.dealTurn(); // should be error
    com.dealFlop(); // should be error
    system("PAUSE");
    com.dealRiver();
    cout << "River: " << com.riverString() << endl;
    cout << "Community: " << com.toString() << endl;
    system("PAUSE");
    
    PokerTable t;
    if(t.getPlayers()[0].getName().compare("") == 0){
       cout << "SEAT EMPTY" << endl;
    }
    else{
       cout << "SEAT NOT EMPTY" << endl;
    }
    system("PAUSE");*/
    cout << "Testing newDeal" << endl;
    
    PokerTable *table = NULL;
    table = new PokerTable();
    table->join("Snuffleupagus");
    table->join("johndoe");
    table->join("mark");
    table->join("George Bush");
    table->join("Shuka");
    table->join("Leah");
    table->join("KillerWhale123");
    table->join("test123456789");
    int dealCards = table->newDeal();
    table->getCommunityCards()->dealFlop();
    table->getCommunityCards()->dealTurn();
    table->getCommunityCards()->dealRiver();
    
     string ss = "";
     
     vector<SeatedPlayer *>* players = table->getPlayersInDeal();
     
     display("Community Cards: ", table->getCommunityCards()->toString());

     for(int ind = 0; ind < players->size(); ind++){
        if(!(*players)[ind]->isFolded()){
           string name = (*players)[ind]->getName();
           string cards = (*players)[ind]->getHand().toString();
           display(name, cards);
           
           Hand h = (*players)[ind]->getHand();
           HoldemHand * hh = static_cast<HoldemHand*>(&h);
           //display("After casting", hh->toString());
           unsigned int assess = hh->assess(table->getCommunityCards());
           
           display("Assess", uintToString(assess));
           
           int rank = hand_rank( assess );
           display("Poker rank", intToString(rank));
           
           display("Poker hand", rankToString(rank));
        }
     }    
    
    table->actionIndex = 7;
    (*table->getPlayers())[0].betChips(431);
    
    (*table->getPlayers())[1].betChips(321);
    
    (*table->getPlayers())[6].betChips(1000);
    
    (*table->getPlayers())[1].fold();
    
     //delete table;
     vector<string> text;
     table->print(text, "Snuffleupagus");
     
     for(int index = 0; index < text.size(); index++){
        cout << text[index] << endl;
     }
     
     system("PAUSE");
     
     table->print(text, "johndoe");
     
     for(int index = 0; index < text.size(); index++){
        cout << text[index] << endl;
     }
     system("PAUSE");
}

int initializeWSA(){
    //Initialization required to use sock.cc unix function calls
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    return WSAStartup(wVersionRequested, &wsaData);  
}

int main(int argc, char *argv[])
{
    /*stringstream ss;
    ss << std::right << std::setw(20) << "test123456 has " << endl;
    cout << ss.str();
    ss.str("");
    ss << std::right << std::setw(20) << "test has " << endl;
    cout << ss.str();
    system("PAUSE");*/
    
    //tester();
    
    /*string test = "     Hello    There   3      1 2 4 6";
    
    stringstream ss(test);
    
    string c;
    string c2;
    string c3;
    string c4;
    string test2;
    
    ss >> c;
    ss >> c2;
    ss >> c3;
    
    cout << c << endl;
    cout << c2 << endl;
    cout << c3 << endl;
    cout << ss.str() << endl;
    
    ss >> c4;
    
    getline(ss, test2);
    
    //cout << c4;
    //cout << test2 << endl;
    
    //ss.str("");
    
    stringstream ss2;
    
    ss2 << c4 << test2;
    
    cout << ss2.str() << endl;
    
    system("PAUSE");*/
    
      srand(time(NULL));  
      
      //ensures it to be much harder to determine the calculate the next pseudo-random number
      //If the user knows that the seed is based on the time.
      //The user will also need to know that RAND_SHUFFLE_KEY calls were made to rand()
      //function before creation of the deck
      /*int arr[52];
      
      for(int i = 0; i < 52; i++){
         arr[i] = 0;
      }
      
      for(long long count = 1; count <= 1000000000; count++){
         arr[rand() % 52]++;
      }     
      
      for(int i = 0; i < 52; i++){
         cout << (i + 1) << " occured " << arr[i] << " times. Probability: " << (double(arr[i])/1000000000.0) << endl;
      }
      
      int highest = 0;
      int lowest = 0;
      
      for (int i = 1; i < 52; i++){
         if(arr[i] > arr[highest]){
            highest = i;
         }
         
         if(arr[i] < arr[lowest]){
            lowest = i;
         }
      }
      
      cout << (highest + 1) << " occured the most number of times" << endl;
      cout << (lowest + 1) << " occured the least number of times" << endl;
      cout << "Probability discrepancy: " << double(arr[highest] - arr[lowest])/1000000000.0 << endl;
      system("PAUSE");*/
    
    int err = initializeWSA(); 
    
    
    if(err != 0) {
        cout << "WSAStartup failed" << endl;
        return err;
    }
    else{
         string password; 
         
         if(argc > 1){
            string password = argv[1];
            
            ICCPokerBot bot(password);
            bot.run();
         }
         else{
            cout << "No password for PokerBot given!" << endl;  
            system("PAUSE");
            return EXIT_FAILURE;
         }
    }
    
    return EXIT_SUCCESS;
}

/*static int ConnectToICC(const string serverName, const string username, const string password, vector<string> &iccStartUp){
	if (icc == NULL) {
		icc = new ICC(serverName, 5068, username, password);
	}
//	srand(time(0));
//	debug = (serverName == "queen.chessclub.com") || (username != MY_ICC_NAME);
//	debug = (username != MY_ICC_NAME);
	if(!icc->connect()){
       cout << "Connection to " << serverName << " with username: " << username << " failed!" << endl;
       return -1;
    }
    
    for(int i = 0; i < iccStartUp.size(); i++){
       icc->sendUnknownCommand(iccStartUp[i]);
    }
	
	while (true) {
		icc->processNextInput(listener);

		const int now = time(NULL);

		if (now >= nNextReincarnate) {
			AutoReincarnate();
		}

		if (now >= nNextUpdate) {
			WriteAll();
			nNextUpdate = now + 10;
		}

		if (now >= nNextReplenish) {
			bloodbath = (randGen.RandInt(20) < 2);
			if (bloodbath) {
				BloodBath(MY_ICC_NAME, MY_ICC_TITLE);
			} else {
				for (unsigned i = 0; i < Players.size(); i++) {
					Players[i].nPF = 1;
				}
				nNextReplenish = now + FIGHT_INTERVAL;
				bloodbath = false;
				//Announce(MY_ICC_NAME, MY_ICC_TITLE, "Each player now has one player fight!", false);
			}
		}

		if (now >= nNextRushReset) {
			for (unsigned i = 0; i < Players.size(); i++) {
				Players[i].ResetWarnings();
			}
		}

		if (now >= nNextAnnounce) {
			icc->sendUnknownCommand("blabber");
			nNextAnnounce += SPAM_FREQUENCY;
		}
	}

	return 0;
}*/
