#ifndef	_CARD_H_
#define	_CARD_H_

#include <string>
#include "Card.h"
#include <vector>

using namespace std;

enum{
   ACE = 1,
   TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
   JACK,
   QUEEN,
   KING
};

enum{
   CLUBS = 0, DIAMONDS = 1, HEARTS = 2, SPADES = 3
};

struct Card {
   Card(int r, int s) : rank(r), suit(s){}
   
   Card(const Card &c) : rank(c.rank), suit(c.suit){}
                
   //Returns back the card in this format: [Xx, Xx]  where X is the rank, and x is the suit
   string toString();     
   
   //Returns back the card as an Ascii Art Text
   void toStringGraphic(vector<string> &text);
   
   unsigned int convertCardToBitNumber();  
   
   int rank;
   int suit;  
   
private:
   void appendRank(string &text);  
};

#endif
