#ifndef _SEATEDPLAYER_H_
#define _SEATEDPLAYER_H_

#include "Hand.h"
#include <string>

using namespace std;

enum BLIND{
   NONE = 0,
   SMALL_BLIND,
   BIG_BLIND, 
   BOTH
};

struct SeatedPlayer{
public:
   SeatedPlayer(string n, ullong start, bool so = false, int missed = NONE, int blind = NONE, bool np = true);
   SeatedPlayer();
   
   void setHand(Hand h){ hand = h; folded = false;}
   
   Hand getHand() { return hand; }
   
   string getName() { return name; }
   
   void setName(string n){ name = n; }
   
   void addChips(int add) { chips += add; }
   
   ullong postBingBlind(ullong bigBlind){
      ullong post = betChips(bigBlind);
      
      missedBlind = NONE;
      newPlayer = false;
      
      return post;
   }
   
   ullong postSmallBlind(ullong smallBlind){
      ullong post = betChips(smallBlind);
      
      missedBlind = NONE;
      
      return post;
   }
   
   ullong betChips(ullong b) { 
      ullong chipsBet;    
          
      if(b >= chips){
         chipsBet = chips;
         chips = 0;
         allin = true;
      }
      else{
         chipsBet = b;
         chips -= b;
      }
      
      bet += chipsBet;
      
      return chipsBet;
   }
   
   ullong collectBet(){
      ullong b = bet;
      bet = 0;
      
      return b;
   }
   
   ullong totalChips() { return chips; }
   
   ullong totalBet() { return bet; }
   
   void leave();
   
   void fold(){ folded = true; }
      
   bool isFolded() { return folded; }
   
   bool sitout;
   
   void clearHand() { hand.clear(); }
   
   string status();
   
   bool isAllIn() { return allin; }
   
   bool canStartHand() { return !sitout && getName() != "" &&  totalChips() > 0; }
   
   bool newPlayer;
   int missedBlind; //Did the player miss a blind?
   int isBlind; //is the player currently in a blind?
private:         
   ullong chips;
   ullong bet;
   Hand hand;
   string name;
   bool folded;
   bool allin;   
};

#endif

