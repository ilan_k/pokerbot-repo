#ifndef _COMMUNITYCARDS_H_
#define _COMMUNITYCARDS_H_

#include "Card.h"
#include "Deck.h"

using namespace std;

class CommunityCards{
public:      
   CommunityCards(Deck * d) : deck(d){
      flop = false;
      turn = false;
      river = false;
   }
   
   //dealFlop deals 3 cards into community
   //as long as flop is false
   bool dealFlop();
   
   //dealTurn deals 1 card into community
   //as long as turn is false
   bool dealTurn();
   
   //dealRiver deals 1 card into community
   //as long as river is false
   bool dealRiver();
   
   //Returns back a string of all the community cards
   //[Xx, Xx, ...]
   string toString();
   
   //Returns sets text with a ascii art graphic of the community cards
   void toStringGraphic(vector<string> &text, int indent = 0);
   
   //Returns back a string of just the flop
   //[Xx, Xx, Xx]
   string flopString();
   
   //[Xx]
   string turnString();
   
   //[Xx]
   string riverString();
    
   //Returns a COPY of the community cards
   //Actual community cards cannot be changed
   vector<Card> getCommunity();
   
      
private:
   CommunityCards(); //disallow creation of CommunityCards without deck
   bool addToCommunity(); 
   bool addToBurn();    
        
   Deck * deck;
   vector<Card> community;
   vector<Card> burnCards; //Are not visible
   bool flop, turn, river;
};

#endif
