#ifndef _HOLDEM_HAND_H_
#define _HOLDEM_HAND_H_

#include "Hand.h"
#include "Card.h"
#include "CommunityCards.h"

#include <vector>
#include <iostream>

using namespace std;

class HoldemHand : public Hand{
public:
      HoldemHand(Card c1, Card c2){
         hand.push_back(c1);
         hand.push_back(c2);
      }
      
      unsigned int assess(CommunityCards * comm);
};

#endif
