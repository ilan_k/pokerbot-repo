#ifndef _HAND_H_
#define _HAND_H_

#include <vector>
#include "Card.h"
#include <string>

typedef unsigned long long ullong;

struct Hand{
      vector<Card> hand;
      
      string toString();
      
      void toStringGraphic(vector<string> &text, int indent = 0);
      
      void clear() { hand.clear(); }
      
      //Hand& operator=(const Hand &other); //Creates hard copy
};

#endif
