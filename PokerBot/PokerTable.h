#ifndef _POKERTABLE_H_
#define _POKERTABLE_H_

#include "CommunityCards.h"
#include "Deck.h"
#include "SeatedPlayer.h"
#include "HoldemHand.h"
#include "icc.h"
#include <vector>
#include <sstream>
#include <iostream>

#define MAX_SEATS 14

#define SUCCESS 0
#define PLAYER_ALREADY_SEATED -1
#define TABLE_FULL -2
#define DEAL_IN_PROGRESS -3
#define TOO_FEW_PLAYERS -4

#define USERNAME_MAXIMUM 15
#define LEFT_INDENT 11
#define BUTTON_SIZE 3

using namespace std;

class PokerTable{
public:
   PokerTable(){
      initializeTable();        
   }
   
   PokerTable(unsigned int s, unsigned int small, unsigned int big){
      initializeTable(s, small, big); 
   }
   
   ~PokerTable(){
      clearCommunity();
      
      clearDeck();
   }
   
   int newDeal(vector<SeatedPlayer*> *playersNowSittingOut = NULL, bool *randomizedButton = NULL);
   
   vector<SeatedPlayer>* getPlayers(){ return &players; }
   
   SeatedPlayer* getPlayer(string name);
   
   vector<SeatedPlayer *>* getPlayersInDeal() {                       
      return &playersInDeal; 
   }
   
   unsigned int totalSeats() { return seats; }
   
   unsigned int totalPlayers() { return numberOfPlayers; }
   
   int join(string name, SeatedPlayer **player = NULL);
   
   int join(string name, ullong chips, SeatedPlayer **player = NULL);
   
   bool remove(const string &name);
   
   void announceHoleCards(ICC* icc, int indent = USERNAME_MAXIMUM + BUTTON_SIZE);
   
   bool announceCommunityCards(ICC* icc);
   
   //Can pass in a vector of <int>
   //Function push_backs all active players indexes into this vector
   int totalActivePlayers(vector<int> *activePlayers = NULL);
   
   CommunityCards * getCommunityCards() { return comm; }
   
   int getPot() { return pot; }
   
   bool dealOver;
   
   void print(vector<string> &text, const string &viewer);
   
   int actionIndex;
   int buttonIndex;  
private:
   void initializeSeats();     
   void initializeTable(unsigned int s = 10, unsigned int small = 5, unsigned int big = 10);  
        
   void clearDeck();
   void clearCommunity();  
   bool shuffleUpAndDeal(vector<SeatedPlayer*> *playersNowSittingOut = NULL, bool *randomizedButton = NULL);
   void printAboveOrBelow(vector<string> &text, vector<int> &playerIndex, int type, const string &viewer);
   void printCommunity(vector<string> &text);
   void printLeftAndRightEdge(vector<string> &text, vector<int> &playerIndex, bool printPot, bool printName, const string &viewer);
   
   void sitOutNoChips(vector<SeatedPlayer*> *playersNowSittingOut = NULL);
   
   int initializeButton(vector<int> &activePlayers, int *activePlayerBtn = NULL, bool *randomizedButton = NULL);
   bool initializeFirstHand(vector<int> &activePlayers, bool *randomizedButton = NULL);
   bool moveButtonPostBlinds(vector<int> &activePlayers, bool *randomizedButton = NULL);
   bool moveButton(vector<int> &activePlayers);
   
   void giveMissedBlinds(int bigBlindIndex);
   void postBigBlind(int bigBlindIndex);
   void postSmallBlind(int smallBlindIndex);
   
   //p1 is a player1 index in activePlayers
   //p2 is a player2 index in activePlayers
   //Assumed that p1 or p2 has the button
   void postBlindsHeadsUp(vector<int> &activePlayers, int p1, int p2);
   void postBlindsHeadsUp(vector<int> &activePlayers);
   
   bool headsUpPlayersHere(vector<int> &activePlayers, int &bb);
   
   //It is assumed that the right most index in bigBlinds is the right most big blind
   //in the game
   void initializeBlinds(const vector<int> &bigBlinds, const vector<int> &smallBlinds);
   
   void playersAbleToPlay(vector<int> &activePlayers);
   
   bool conversionFromHeadsUp(vector<int> &activePlayers);
   
   void dealHandToPlayer(SeatedPlayer * sp);
   
   
   unsigned int numberOfPlayers;  
   unsigned int seats;
   CommunityCards * comm;
   Deck * deck;
   vector<SeatedPlayer> players;
   ullong pot;
   
   unsigned int smallBlind;
   unsigned int bigBlind;
   
   bool twoBigBlinds;
   bool headsUp;
   
   vector<SeatedPlayer *> playersInDeal; 
};

#endif
