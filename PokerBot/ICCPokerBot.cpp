#include "ICCPokerBot.h"
#include "icc.h"

#include <fstream> //For reading and writing profiles
#define PROFILES "profiles.txt"
#define HELP_FILE "help.txt"
#define STARTING_CHIPS 10000

#include "testing.h"

//#define BOARD_SLEEP_TIME 0
#define BOARD_SLEEP_TIME 2000
//#define SHOWDOWN_SLEEP_TIME 0
#define SHOWDOWN_SLEEP_TIME 1000

#include <Windows.h>

ICCPokerBot::ICCPokerBot(string p){
   icc = NULL;
   
   servername = "chessclub.com";
   username = "PokerBot";
   password = p;
   table = NULL;
   icc = NULL;
   
   connected = connect();
}

ICCPokerBot::ICCPokerBot(string sn, string u, string p){
   icc = NULL;
   
   servername = sn;
   username = u;
   password = p;
   table = NULL;
   icc = NULL;
   
   connected = connect();
}

void ICCPokerBot::closeConnection(){
   if(icc != NULL){
      delete icc;
      
      icc = NULL;
   }
}

void ICCPokerBot::closeTable(){
   if(table != NULL){
      delete table;
   }
   
   table = NULL;
}

bool ICCPokerBot::connect(){
   
   //Remove any connection and tables
   //prior to logging in+
   closeConnection();
   closeTable();
     
   if(icc == NULL){
      icc = new ICC(servername, 5068, username, password);
      listener.setBot(this);
   }
   
   if(!icc->connect()){
      cout << "Connection to " << servername << " with username: " << username << " failed!" << endl;
      return false;
   }
   
   return true;
}

int ICCPokerBot::run(){
   if(connected){
      vector<string> iccStartUp;
      iccStartUp.push_back("set interface PokerBot 1.0");
      
      running = true;
      
      //initial ICC setup commands
      for(int i = 0; i < iccStartUp.size(); i++){
         icc->sendUnknownCommand(iccStartUp[i]);
      }
      
      if(loadProfiles()){ //initializes profiles from the profile file
         loadTable();
      
         while (running) {
            icc->processNextInput(listener);
         }
      
         closeConnection();
         saveProfiles();
      }
      
      return 0;
   }
   else{
      return -1;
   }
}

void ICCPokerBot::tellInvalid(const string &teller){
   stringstream ss;  
   ss << "I did not understand your message. Please check to make sure you typed everything correctly, "
      << "or \"tell " << username << " help\" for a list of commands.";
   tell(teller, ss.str());
}

bool ICCPokerBot::loadProfiles(){
   profiles.clear();
   
   ifstream input(PROFILES);
   string line;
   
   
   if(input.is_open()){
      while(input.good()){
         getline(input, line);
         
         stringstream ss(line);
         
         pair<string, ullong> profile;
         
         ss >> profile.first;
         
         if(profile.first != ""){
            try{
               if((ss >> profile.second).fail()){
                  throw -1;
               }
            }
            catch(int e){
               stringstream ss2;
               ss2 << "Error! " << PROFILES << " input file was not in correct format. Could not load Profiles successfully, ending program" << endl;
               cout << ss2.str();
               
               tell(MY_MANAGER, ss2.str(), false, false);
               Sleep(2000);
               
               if(DEBUG_POKER_BOT) system("PAUSE");
               
               running = false;
               input.close();
               return false;
            }
            
            profiles.push_back(profile);
         }
         
         //ss >> profile.second;
      }                 
                       
      input.close();
   }
   else{
      cout << "Input file did not exist, Profiles are empty" << endl;
   }
   
   return true;
}

void ICCPokerBot::saveProfiles(){
   ofstream output(PROFILES);
   
   stringstream ss;
   
   for(int ind = 0; ind < profiles.size(); ind++){
      ss.str("");
      
      ss << profiles[ind].first << " " << profiles[ind].second << endl;
      
      output << ss.str();
   }
   
   output.close();
   
   cout << "Saving profiles to " << PROFILES << " was successfully completed!" << endl;
   system("PAUSE");
}

void ICCPokerBot::loadTable(){
   bool created;  
     
   //Only open a table if there isn't a current table
   if(table == NULL){
      table = new PokerTable();
      
      int s = table->totalSeats();
      int p = table->totalPlayers();
      
      cout << "seats: " << s << ", players: " << p << endl;      
      
      cout << "Table pot is " << table->getPot() << endl;      
      
      stringstream ss;
      
      ss << "New poker table has been opened! Type: \"tell " << username << " join\" to enter. Max seats: "
         << s << ". Total Players: " << p;
         
      if(TESTING) tell(MY_MANAGER, ss.str(), true, true); else tell(MY_CHANNEL, ss.str(), true, true);
         
            
      created = true;
   } 
   else{
      created = false;
   }
}

string ICCPokerBot::playersAtTable(){
   stringstream ss;
   
   ss << "We now have " << table->totalActivePlayers() << " active players. (" 
      << table->totalPlayers() << " total).";
   
   return ss.str();
}

void ICCPokerBot::joinTable(const string &teller){
   SeatedPlayer *player = NULL;  
     
   int join = table->join(teller, &player);
   
   stringstream ss;
   
   switch(join){
      case PLAYER_ALREADY_SEATED:
         ss << "You are already at the table! ";
         
         if(player != NULL){
            ss << player->status();
         }
         break;
      case TABLE_FULL:
         ss << "Sorry, poker table is currently full!";
         break;
      default:
         ss << "You have successfully joined the table! ";
         
         if(player != NULL){
            ss << player->status();
         }
     
      if(!TESTING) tell(teller, ss.str(), true, true); 
         
         stringstream ss2;  
         ss2 << "Player: " << teller << "(" << player->totalChips() << ") has joined - " << playersAtTable();
           
      if(TESTING) tell(MY_MANAGER, ss2.str(), true, true); else tell(MY_CHANNEL, ss2.str(), true, true); 
   }
   
   tell(teller, ss.str());
}

void ICCPokerBot::statusRequested(const string &teller){
   SeatedPlayer *player = table->getPlayer(teller);
   
   if(player == NULL){
      tell(teller, "You are currently not at the table!");
   }
   else{
      tell(teller, player->status(), true, true);
   }
}

void ICCPokerBot::sitoutRequested(const string &teller){
   SeatedPlayer *player = table->getPlayer(teller);
   
   if(player == NULL){
      tell(teller, "You are currently not at the table!");
   }
   else{
      if(player->sitout){
         tell(teller, "You are already currently sitting out");
      }
      else{
         player->sitout = true;
         tell(teller, "You are now sitting out");
         
         stringstream ss;
         ss << "Player: " << teller << "(" << player->totalChips() << ") is now sitting out - " << playersAtTable();
         
         if(TESTING) tell(MY_MANAGER, ss.str(), true, true); else tell(MY_CHANNEL, ss.str(), true, true);
      }
   }
}

void ICCPokerBot::sitinRequested(const string &teller){
   SeatedPlayer *player = table->getPlayer(teller);
   
   if(player == NULL){
      tell(teller, "You are currently not at the table!");
   }
   else{
      if(!player->sitout){
         tell(teller, "You are already currently sitting in");
      }
      else{
         player->sitout = false;
         tell(teller, "You are now sitting in");
         
         stringstream ss;
         ss << "Player: " << teller << "(" << player->totalChips() << ") is now sitting in - " << playersAtTable();
         
         if(TESTING) tell(MY_MANAGER, ss.str(), true, true); else tell(MY_CHANNEL, ss.str(), true, true);
      }
   }
}

int ICCPokerBot::newDeal(const string &teller){
   vector<SeatedPlayer*> playersNowSittingOut;
   bool randomizedButton = false;
   
   int dealCards = table->newDeal(&playersNowSittingOut, &randomizedButton);
   
   cout << "dealCards is " << dealCards << endl;
   
   for(int ind = 0; ind < playersNowSittingOut.size(); ind++){
      stringstream ss;
      
      ss << playersNowSittingOut[ind]->getName() << " is now sitting out." << endl;
      tellTable(ss.str(), true, false);
   }
   
   if(randomizedButton){
      stringstream ss;
      
      ss << ((*table->getPlayers())[table->buttonIndex]).getName() << " has won the dealer button (D)";
      
      tellTable(ss.str(), true, false);
   }   
   
   cout << "After table->newDeal()" << endl;
   if(DEBUG_POKER_BOT) system("PAUSE");
   
   if(dealCards == SUCCESS){
      if(TESTING) tell(MY_MANAGER, "A new deal is now in progress! Dealing hole cards now!", true, true); 
      else tell(MY_CHANNEL, "A new deal is now in progress! Dealing hole cards now!", true, true);
      
      table->announceHoleCards(icc);
      
      vector<SeatedPlayer *>* playersInDeal = table->getPlayersInDeal();
      
      for(int ind = 0; ind < playersInDeal->size(); ind++){
         cout << (*playersInDeal)[ind]->getName() << " " << (*playersInDeal)[ind]->getHand().toString() << endl;
      } 
      
      cout << "After saying player hands" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");
   }
   else{
      if(dealCards == DEAL_IN_PROGRESS){
         tell(teller, "A hand is currently in progress! Could not initialize a new deal");
      }
      else if(dealCards == TOO_FEW_PLAYERS){
         tell(teller, "Too few players! Could not initialize a new deal");
      }
   }
   
   return dealCards;
}

void ICCPokerBot::fullDeal(const string &teller){   
   if(newDeal(teller) == SUCCESS){
      displayTableAll(teller);
            
      cout << "After newDeal(teller)" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");
               
      Sleep(BOARD_SLEEP_TIME);
      dealFlopRequested(teller);
               
      cout << "After dealFlopRequested(teller)" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");
               
      Sleep(BOARD_SLEEP_TIME);
      dealTurnRequested(teller);
                  
      cout << "After dealTurnRequested(teller)" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");               
               
      Sleep(BOARD_SLEEP_TIME);
      dealRiverRequested(teller);
      cout << "After dealRiverRequested(teller)" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");                  
                  
      Sleep(SHOWDOWN_SLEEP_TIME);
      showDownRequested(teller);
               
      cout << "After showDownRequested(teller)" << endl;
              
      if(DEBUG_POKER_BOT) system("PAUSE");
   }
}

void ICCPokerBot::quitTable(const string &teller){
     if(table != NULL){
        bool leave = table->remove(teller);
     
        if(leave){
           tell(teller, "You have been successfully removed from the table"); 
           
           stringstream ss;
           
           ss << "Player: " << teller << " has left - " << playersAtTable();
           
           if(TESTING) tell(MY_MANAGER, ss.str(), true, true); else tell(MY_CHANNEL, ss.str(), true, true);
        }
        else{
           tell(teller, "You are currently not at the table!");
        }
     }
     else{
        tell(teller, "Poker Table has not yet been created!");
     }
}

void ICCPokerBot::notReady(const string &teller){
   stringstream ss;
   
   ss << username << " has not yet initialized. Please talk to my manager " << MY_MANAGER;
   tell(teller, ss.str());
}

void ICCPokerBot::dealFlopRequested(const string &teller){
   cout << "Inside dealFlopRequested()" << endl;
   if(DEBUG_POKER_BOT) system("PAUSE");
     
   if(table->getCommunityCards()->dealFlop()){
      cout << "inside if(table->getCommunityCards()->dealFlop())" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");                                        
      stringstream ss;
      //ss << std::right << std::setw(25) << "Dealing the Flop: " << table->getCommunityCards()->toString();
      
      cout << "before tellTable() in dealFlopRequested()" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");   
         
      tellTable("Dealing the Flop: ", true, false);
      
      vector<string> graphic;
      table->getCommunityCards()->toStringGraphic(graphic, USERNAME_MAXIMUM);
      
      for(int ind = 0; ind < graphic.size(); ind++){
         tellTable(graphic[ind], true, false);
      }
      
      
      cout << "after tellTable() in dealFlopRequested()" << endl;
      if(DEBUG_POKER_BOT) system("PAUSE");
   }
   else{
      tell(teller, "Could not deal flop! Flop was already dealt");
   }
}

void ICCPokerBot::dealTurnRequested(const string &teller){
   if(table->getCommunityCards()->dealTurn()){
      stringstream ss;
      //ss << std::right << std::setw(25) << "Dealing the Turn: " << table->getCommunityCards()->toString();
      //tell(MY_CHANNEL, ss.str(), true, true); 
      
      tellTable("Dealing the Turn: ", true, false);
      
      vector<string> graphic;
      table->getCommunityCards()->toStringGraphic(graphic, USERNAME_MAXIMUM);
      
      for(int ind = 0; ind < graphic.size(); ind++){
         tellTable(graphic[ind], true, false);
      }
   }
   else{
      tell(teller, "Could not deal turn!");
   }
}

void ICCPokerBot::dealRiverRequested(const string &teller){
   if(table->getCommunityCards()->dealRiver()){
      stringstream ss;
      //ss << std::right << std::setw(25) << "Dealing the River " << table->getCommunityCards()->toString();
      //tell(MY_CHANNEL, ss.str(), true, true); 
      
      tellTable("Dealing the River: ", true, false);
      
      vector<string> graphic;
      table->getCommunityCards()->toStringGraphic(graphic, USERNAME_MAXIMUM);
      
      for(int ind = 0; ind < graphic.size(); ind++){
         tellTable(graphic[ind], true, false);
      }
   }
   else{
      tell(teller, "Could not deal river!");
   }
} 

void ICCPokerBot::showDownRequested(const string &teller){
     table->dealOver = true;
     
     displayTableAll(teller);
     
     stringstream ss;
     
     const int WIDTH = 18;
     
     vector<SeatedPlayer *>* players = table->getPlayersInDeal();
     cout << "players in deal size: " << players->size() << endl;
     
     if(TESTING) tell(MY_MANAGER, " ", true, true); else tell(MY_CHANNEL, " ", true, true);
     
     tellTable(ss.str(), true, true);
     
     vector< pair< pair<short, string>, string > > playerHands;
     
     for(int ind = 0; ind < players->size(); ind++){
        if(!(*players)[ind]->isFolded()){
           Hand h = (*players)[ind]->getHand();
           short assess = static_cast<HoldemHand*>(&h)->assess(table->getCommunityCards());
           string name = (*players)[ind]->getName();
           string handDesc = rankToString(assess, ALL_POKER_HANDS_FILE);
           
           playerHands.push_back(make_pair(pair<short, string>(assess, handDesc), name));
           
           ss.str("");
                                    
           ss << "     " << name;
           
           for(int count = 1; count < WIDTH - name.size(); count++) ss << " "; 
                         
           ss << " shows " <<  (*players)[ind]->getHand().toString() 
              << ", " << handDesc;
           //tell(MY_CHANNEL, ss.str(), true, false);
           tellTable(ss.str(), true, false);                   
           
           //This next line causes problems?
           //tell(MY_CHANNEL, " ", true, false); //Sends a blank line >
        }
     }
     
     int bestHand = 9999;
     
     //Decide the best poker hand
     for(int ind = 0; ind < playerHands.size(); ind++){
        if(playerHands[ind].first.first < bestHand){
           bestHand = playerHands[ind].first.first;
        }
     }
     
     vector< pair< pair<short, string>, string > > winners;
     
     for(int ind = 0; ind < playerHands.size(); ind++){
        if(playerHands[ind].first.first == bestHand){
           winners.push_back(playerHands[ind]);
        }
     }
     
     int pot = table->getPot();
     cout << "pot is " << pot << endl;
     
     //One person has won the entire hand
     if(winners.size() == 1){
        ss.str(""); 
        
        ss << winners[0].second << " wins the pot (" << pot << ") with "; 
        
        switch(hand_rank(winners[0].first.first)){
           case ONE_PAIR:
           case STRAIGHT:
           case FLUSH:
           case STRAIGHT_FLUSH:
              ss << "a ";
        }
        
        ss << winners[0].first.second;
        
        tellTable(ss.str(), true, false); 
     }
     //Split Hand
     else{
        ss.str("");
        
        for(int ind = 0; ind < winners.size() - 1; ind++){
           ss << winners[ind].second << ", ";
        }
        
        ss << "and " << winners[winners.size() - 1].second << " split the pot (" << pot << ") with "; 
        
        switch(hand_rank(winners[0].first.first)){
           case ONE_PAIR:
           case STRAIGHT:
           case FLUSH:
           case STRAIGHT_FLUSH:
              ss << "a ";
        }
        
        ss << winners[0].first.second;
        
        tellTable(ss.str(), true, false); 
     }
} 

bool ICCPokerBot::isRegistered(const string &teller){
   for(int ind = 0; ind < profiles.size(); ind++){
      if(profiles[ind].first == teller){
         return true;
      }
   }
   
   return false;
}

void ICCPokerBot::ParseTell(const string &teller, const string &tellerTitles, const string &t){
   stringstream sstt(tellerTitles);
   string token;  
     
   //The following ignores any tells from (C) or (TD)
   //It is used as a precaution to avoid constant spamming between two Computers
   while(getline(sstt, token, ' ')){
      if(token == "C" || token == "TD"){
         if(DEBUG_POKER_BOT){ cout << "Ignoring teller, as they are either a (C) or a (TD)" << endl; }
         return;
      }
   }   
     
   bool isManager = (teller == MY_MANAGER);
   bool registered = isRegistered(teller);
   
   if(DEBUG_POKER_BOT){ cout << "isManager is " << isManager << endl; cout << "registered is " << registered << endl; }
   
   //string low = toLowerCase(t);
        
   stringstream ss(t);   
                                     
   vector<string> command;
        
   while(getline(ss, token, ' ')){
      command.push_back(token);
   }
            
   stringstream ss2;
   ss2 << "User: " << teller << " sent me: " << t;   
   tell(MY_MANAGER, ss2.str(), true, true);
   
   if(command.size() == 1 && command[0].compare("help") == 0){ //Priority is to give help first even if user is not registered
      helpRequested(teller);
   }   
   else if(!registered){
      if(command.size() == 1 && command[0].compare("register") == 0){
         pair<string, ullong> newProfile(teller, STARTING_CHIPS);
         profiles.push_back(newProfile);                     
         
         stringstream ss2;
         
         ss2 << "You have successfully registered! You have a total of " << STARTING_CHIPS << " play chips in your bankroll";
         tell(teller, ss2.str(), false, false);
      }
      else{
         stringstream ss2;
         ss2 << "Hello, " << teller << ". Please do \'tell " << username << " register\'.";
         cout << "Sending to " << teller << ": " << ss2.str() << endl;
         tell(teller, ss2.str(), false, false);
      }
   }
   else if(command.size() > 0){ //User is now registered
      if(command.size() == 1){
         if(command[0].compare("register") == 0){
            stringstream ss2;
            ss2 << "Hello, " << teller << ". You are already registered. Tell me \'help\' if you have problems.";
            tell(teller, ss2.str(), false, false);
         }
         else if(command[0].compare("display") == 0){
            displayTable(teller);
         }
         else if(command[0].compare("deal") == 0 && isManager){
            fullDeal(teller);
         }
         else if(command[0].compare("begin") == 0){
         }
         else if(command[0].compare("sitin") == 0){
            sitinRequested(teller);
         }
         else if(command[0].compare("join") == 0){
            joinTable(teller);
         }
         else if(command[0].compare("status") == 0){
            statusRequested(teller);
         }
         else if(command[0].compare("sitout") == 0){
            sitoutRequested(teller);
         }
         else if((command[0].compare("quit") == 0) || (command[0].compare("exit") == 0) || (command[0].compare("leave") == 0)){
            quitTable(teller);
         }
         else if(command[0].compare("showdown") == 0 && isManager){
            showDownRequested(teller);
         }
         else if(command[0].compare("shutdown") == 0 && isManager){
           if(TESTING) tell(MY_MANAGER, "is now shutting down. Good Bye!", true, true); else tell(MY_CHANNEL, "is now shutting down. Good Bye!", true, true); 
           
           Sleep(2000); //Ensure last tell has been successfully completed before shuttingdown
           running = false;
         }
         else{
            tellInvalid(teller);
         }
      }
      else if(command.size() == 2){
         if(command[0].compare("sit") == 0 && command[1].compare("out") == 0){
            sitoutRequested(teller);
         }
         else if(command[0].compare("sit") == 0 && command[1].compare("in") == 0){
            sitinRequested(teller);
         }
         else if(command[0].compare("display") == 0 && command[1].compare("all") == 0 && isManager){
            displayTableAll(teller);
         }
         else if(command[0].compare("deal") == 0 && command[1].compare("flop") == 0 && isManager){
            dealFlopRequested(teller);
         }
         else if(command[0].compare("deal") == 0 && command[1].compare("turn") == 0 && isManager){
            dealTurnRequested(teller);
         }
         else if(command[0].compare("deal") == 0 && command[1].compare("river") == 0 && isManager){
            dealRiverRequested(teller);
         }
         else if(command[0].compare("join") == 0 && isManager){
            joinTable(command[1]);
         }
         else{
            tellInvalid(teller);
         }
      }
      else if(command.size() == 3){
         if(command[0].compare("forcebet") == 0 && isManager){
            SeatedPlayer * player = table->getPlayer(command[1]);
            
            cout << "In forcebet, player specified: " << command[1] << ". Bet specified: " << command[2] << endl;
            
            if(player == NULL){
               stringstream ss2;  
               ss2 << "Player: \"" << command[1] << "\" is not at the table!";
               
               cout << "ss2.str() is " << ss2.str() << endl;
               tell(teller, ss2.str());
            }
            else{
               ullong bet;
               
               try{
                  cout << "command[2] is " << command[2] << endl;
                   
                  if((stringstream(command[2]) >> bet).fail()){
                     throw -1;
                  }
                     
                  cout << "bet is " << bet << endl;
                  ullong chipsBet = player->betChips(bet);
                   
                  stringstream ss2;
                  ss2 << "You have bet a total of " << chipsBet << ". Chips Left: " << player->totalChips();
                  tell(teller, ss2.str());
               }
               catch(int e){
                  tell(teller, "Specified bet could not be converted!");
               }
            }
         }
         else{
            tellInvalid(teller);
         }
      }
      else{
         if(isManager){
            if(command[0].compare("spoof") == 0){
               stringstream ss2(t);
               stringstream ss3;
               string first;
               string rest;
               ss2 >> first; //get rid of spoof
               ss2 >> first;
               
               getline(ss2, rest);
               ss3 << first << rest;
                  
               icc->sendUnknownCommand(ss3.str());
            }
            else{
               tellInvalid(teller);
            }
         }
         else{
            tellInvalid(teller);
         }
      }               
   }
   //Sent Bot a blank tell
   else{
      tellInvalid(teller);
   }
}

void ICCPokerBot::helpRequested(const string &teller){
   ifstream input;
   
   input.open(HELP_FILE); 
   
   string line;
   
   if(input.is_open()){
      while(input.good()){
         getline(input, line);
         
         tell(teller, line, true, false);
      }                 
                       
      input.close();
   }
   else{
   } 
}

void ICCPokerBot::tell(string user, string mess, bool qtell, bool includeBotName){
   if(qtell){
      //qtell has the ability to not display bot name
      //IE can just print
      // > <message>
      stringstream ss;
      
      if(includeBotName){
         ss << username << " " << mess;
      }
      else{
         ss << mess;
      }
      
      if(DEBUG_POKER_BOT){ cout << "Sending qtell to " << user << ": " << mess << endl; }
      icc->qtell(user, ss.str());
   }
   else{
      if(DEBUG_POKER_BOT){ cout << "Sending tell to " << user << ": " << mess << endl; }
      icc->tell(user, mess);
   }
}

void ICCPokerBot::tell(Channel channel, string mess, bool qtell, bool includeBotName){
   if(qtell){
      //qtell has the ability to not display bot name
      //IE can just print
      // > <message>
      stringstream ss;
      
      if(includeBotName){
         ss << username << " " << mess;
      }
      else{
         ss << mess;
      }
      
      if(DEBUG_POKER_BOT){ cout << "Sending qtell to " << channel << ": " << mess << endl; }
      icc->qtell(channel, ss.str());
   }
   else{
      if(DEBUG_POKER_BOT){ cout << "Sending tell to " << channel << ": " << mess << endl; }
      icc->tell(channel, mess);
   }
}

void ICCPokerBot::tellTable(string mess, bool qtell, bool includeBotName){
   if(DEBUG_POKER_BOT){ cout << "in tell Table()" << endl; system("PAUSE"); } 
   vector<SeatedPlayer>* players = table->getPlayers();  
     
   if(DEBUG_POKER_BOT){ cout << "after table->getPlayers() in tellTable" << endl; system("PAUSE"); }
   
   if(DEBUG_POKER_BOT){ cout << "players->size() is " << players->size() << endl; system("PAUSE"); }
     
   for(int ind = 0; ind < players->size(); ind++){
      if((*players)[ind].getName() != ""){
         tell((*players)[ind].getName(), mess, qtell, includeBotName);
      }
   }
   
   if(DEBUG_POKER_BOT){ cout << "at end of tellTable Function" << endl; system("PAUSE"); }
}

void ICCPokerBot::displayTable(const string &teller){
   vector<string> text;  
     
   table->print(text, teller);  
     
   tell(teller, "", true, true);
   
   for(int i = 0; i < text.size(); i++){
      tell(teller, text[i], true, false);
   }
}

void ICCPokerBot::displayTableAll(const string &teller){
   vector<SeatedPlayer>* players = table->getPlayers();
   
   for(int i = 0; i < players->size(); i++){
      if((*players)[i].getName() != ""){
         displayTable((*players)[i].getName());
      }
   }
}
