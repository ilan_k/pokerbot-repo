// Datagram!
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

#include "datagram.h"

Datagram::Datagram(const char *s) :
	argc(0) {
	// Get the type skipping the initial \031(.
	type = atoi(s + 2);

	// Make a local copy of the datagram string.
	// Remove the \031( and \031).
	int len = strlen(s);
	if (len >= DG_MAX_LENGTH) {
		fprintf(stderr, "Datagram too long!\n");
		len = DG_MAX_LENGTH - 1;
	}
	strncpy(dg, s + 2, len - 4);
	dg[len - 4] = 0;

	// Now parse the args.  Be careful with strings.
	// 8/29/99 Modify parsing: Allow multiple spaces between args,
	//    spaces at the end of the datagram, and no spaces between string args.
	char *p = strchr(dg, ' ') + 1;
	while (1) {
		if (p[0] == '{') {
			arg[argc++] = p + 1; // Skip the {
			p = strchr(p, '}');
			p[0] = 0; // Replace the } with NULL.
		} else if (p[0] == '\031' && p[1] == '{') {
			arg[argc++] = p + 2; // Skip the \031{
			p = strchr(p + 2, '\031');
			p[0] = 0; // Replace the \031 with NULL.
			p[1] = ' '; // Replace the } with a space.
		} else {
			arg[argc++] = p;
			p = strchr(p, ' ');
			if (!p) // Done?
				return;
			p[0] = 0; // Replace the space with NULL.
		}

		while (1) {
			p++;
			if (!(*p)) // Done?
				return;
			if (*p != ' ') // Look for a non-space.
				break;
		}
	}
}

const char *Datagram::Arg(int i) {
	if (i >= argc || i < 0)
		return 0;

	return arg[i];
}
