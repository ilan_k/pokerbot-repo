#include "utils.h"

string toLowerCase(const string &str){
   string lower = "";
   
   for(unsigned int ind = 0; ind < str.size(); ind++){
      lower += (char)tolower(str[ind]);
   }
   
   return lower;
}
